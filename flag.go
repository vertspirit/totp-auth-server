package main

import (
    "flag"
)

type args struct {
    host      string
    port      int
    config    string
    subpath   string
    syslog    bool
    graceful  bool
    debug     bool
}

func parseFlags() *args {
    arg := &args{}
    flag.StringVar(&arg.config, "config", "config/config.yaml", "The configuration file of server.")
    flag.StringVar(&arg.config, "c", "config.yaml", "The configuration file of server. (shorten)")
    flag.StringVar(&arg.host, "host", "0.0.0.0", "The ip address of hostname of server.")
    flag.StringVar(&arg.host, "h", "0.0.0.0", "The ip address or hostname of server. (shorten)")
    flag.IntVar(&arg.port, "port", 9987, "The port of service.")
    flag.IntVar(&arg.port, "p", 9987, "The port of service. (shorten)")
    flag.StringVar(&arg.subpath, "subpath", "", "The subpath prefix for api server.")
    flag.StringVar(&arg.subpath, "b", "", "The subpath prefix for api server. (shorten)")
    flag.BoolVar(&arg.syslog, "syslog", false, "Enable the logging by syslog to local or remote.")
    flag.BoolVar(&arg.syslog, "s", false, "Enable the logging by syslog to local or remote. (shorten)")
    flag.BoolVar(&arg.graceful, "graceful", false, "Use graceful mode to start up this server.")
    flag.BoolVar(&arg.graceful, "g", false, "Use graceful mode to start up this server. (shorten)")
    flag.BoolVar(&arg.debug, "debug", false, "Run in debug mode to analyze performace by pprof.")
    flag.BoolVar(&arg.debug, "d", false, "Run in debug mode to analyze performace by pprof. (shorten)")
    flag.Parse()
    return arg
}
