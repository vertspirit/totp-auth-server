package main

import (
    "fmt"
    "strings"
    "bufio"
    "os"
    "time"
    "strconv"
    "image/color"
    "github.com/mdp/qrterminal/v3"

    "totp-auth-server/pkg/totp"
)

func showConsoleQRCode(data string, config *qrterminal.Config) {
    qrterminal.GenerateWithConfig(data, *config)
}

func main() {
    var key        string
    var createPng  bool

    // createa a buffer for stdinWE
    reader := bufio.NewReader(os.Stdin)

    fmt.Print("Do you want to create png file for totp secret qrcode [y/N]: ")
    cpng, _ := reader.ReadString('\n')
    pngFile := strings.TrimSpace(cpng)
    switch pngFile {
    case "yes", "y":
        createPng = true
    default:
        createPng = false
    }

    fmt.Print("Enter algorithm to use [sha1]: ")
    al, _ := reader.ReadString('\n')
    alg := strings.TrimSpace(al)
    if alg == "" {
        alg = "sha1"
    }

    fmt.Print("Enter time step in second [30]: ")
    interval, _ := reader.ReadString('\n')
    if interval == "\n" {
        interval = "30"
    }
    ts, err := strconv.ParseInt(strings.TrimSpace(interval), 10, 64)
    if err != nil {
        fmt.Println(err)
    }

    fmt.Print("Enter digits for TOTP [6]: ")
    dig, _ := reader.ReadString('\n')
    if dig == "\n" {
        dig = "6"
    }
    length, err := strconv.Atoi(strings.TrimSpace(dig))
    if err != nil {
        fmt.Println(err)
    }

    // Choose a generated secret key or input key by user
    fmt.Print("Generate the random secret key [Y/n]: ")
    keygen, _ := reader.ReadString('\n')
    answer := strings.ToUpper(strings.TrimSpace(keygen))
    switch answer {
    case "N", "NO":
        fmt.Print("Enter secret key: ")
        key, _ = reader.ReadString('\n')
        fmt.Printf("Secret Key: %s\n", key)
    default:
        // generate a random secret key
        key = totp.GenerateSecretKey(32)
        label := "totp-auth"
        otpauth := "otpauth://totp/" + label +
                   "?secret=" + key + "&algorithm=" + strings.ToUpper(alg) +"&digits=" + strings.TrimSpace(dig) +
                   "&period=" + strings.TrimSpace(interval) + "&count=0"
        // TOTP URL example
        // otpauth://hotp/TOTP-AUTH?secret=r7s...tsdf&algorithm=SHA1&digits=6&period=30&count=0
        // otpauth://totp/TOTP-AUTH?secret=r7s...tsdf
        fmt.Printf("Secret Key: %s\n", otpauth)

    grcConfig := &qrterminal.Config{
        Level:       qrterminal.M,
        Writer:      os.Stdout,
        BlackChar:   qrterminal.BLACK,
        WhiteChar:   qrterminal.WHITE,
        QuietZone:   1,
    }
    showConsoleQRCode(otpauth, grcConfig)

    if createPng {
        fmt.Println("Saved the QRcode to qr.png file.")
        CreateQRCodePng(otpauth, "", "", 0)
    }
  }

    token := totp.GenerateTOTP(strings.TrimSpace(key), ts, length, strings.ToLower(alg))
    fmt.Printf("TOTP: %s\n", token)
}
