// +build doc

package main

import (
    ginSwagger "github.com/swaggo/gin-swagger"
    swaggerFiles "github.com/swaggo/files"
    _ "totp-auth-server/docs"
)

func init() {
    // You cloud disable the swagger if the environment variable DISABLE_SWAGGER Set to any value.
    swagHdler = ginSwagger.DisablingWrapHandler(swaggerFiles.Handler, "DISABLE_SWAGGER")
}
