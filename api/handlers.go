package api

import(
    "fmt"
    "log"
    "net/http"
    "net/url"
    "bytes"
    "strconv"
    "encoding/json"
    "github.com/gin-gonic/gin"

    "totp-auth-server/pkg/totp"
    "totp-auth-server/pkg/keygen"
)

// @Summary Show Server Information
// @Description get information for this totp auth server
// @Tags info
// @Accept json
// @Produce json
// @Success 200 {string} string "Success"
// @Failure 400 {string} string "Bad request"
// @Failure 401 {string} string "Unauthorized"
// @Failure 403 {string} string "Forbidden"
// @Failure 404 {string} string "Not found"
// @Router /info [get]
func (tas *TotpApiSettings) showInfoHandler(c *gin.Context) {
    c.JSON(http.StatusOK, gin.H {
        "name": "TOTP Auth Server",
        "version": tas.Version,
        "client-ip": c.ClientIP(),
        "x-forwarded-for": c.GetHeader("X-Forwarded-For"),
    })
    c.Abort()
}

// @Summary API Keygen
// @Description generate random api key, api secret,totp secret key
// @Tags toolkit
// @Accept json
// @Produce json
// @Success 200 {string} string "Success"
// @Failure 400 {string} string "Bad request"
// @Failure 401 {string} string "Unauthorized"
// @Failure 403 {string} string "Forbidden"
// @Failure 404 {string} string "Not found"
// @Router /keygen [get]
func (tas *TotpApiSettings) generateKeyHandler(c *gin.Context) {
    totpSecretKey := totp.GenerateSecretKey(16)
    apiKey := keygen.GenerateRandomSalt(16)
    apiSecret := keygen.GenerateRandomSalt(8) // salt
    _, uid, err := keygen.GenerateUUID("V4", false)
    if err != nil {
        c.AbortWithError(http.StatusBadRequest, err)
        return
    }
    c.JSON(http.StatusOK, gin.H {
        "totp-secret-key": totpSecretKey,
        "api-key": apiKey,
        "api-secret": apiSecret,
        "uuid": uid,
    })
}

// @Summary Show User Key QRcode
// @Description get user secret key and show it with qrcode
// @Tags user
// @Accept application/json
// @Produce image/png
// @Security ApiKeyAuth
// @Security ApiSecretAuth
// @Security ApiUserAuth
// @Param name path string true "User Name"
// @Success 200 {string} string "Success"
// @Failure 400 {string} string "Bad request"
// @Failure 401 {string} string "Unauthorized"
// @Failure 403 {string} string "Forbidden"
// @Failure 404 {string} string "Not found"
// @Router /manage/user/{name}/qrcode [get]
func (tas *TotpApiSettings) showUserKeyQRcodeHandler(c *gin.Context) {
    name, err := url.QueryUnescape(c.Param("name"))
    if err != nil {
        c.AbortWithStatusJSON(http.StatusBadRequest, gin.H {
            "operation": "fail",
            "message":   fmt.Sprintf("URL Decode to Query String Error: %v", err),
        })
        return
    }
    secret, err := tas.KeyStorage.GetSecretByUsernameCtx(c, name)
    if err != nil {
        log.Printf("Unable to get secret key by username %s: %v\n",name, err)
        c.AbortWithStatusJSON(http.StatusBadRequest, gin.H {
            "status": "fail",
            "message": fmt.Sprintf("Unable to get secret key by username %s.", name),
        })
        return
    }
    label := fmt.Sprintf("totp-%s", name)
    url := totp.CreateTotpAuthUrl(
        label,
        secret,
        tas.Controller.TotpProperies.Algorithm,
        tas.Controller.TotpProperies.TimeStep,
        tas.Controller.TotpProperies.Digits,
        0,
    )
    buffer := new(bytes.Buffer)
    defer buffer.Reset()
    raw, err := totp.CreateQRCodePngRaw(url, tas.QRcodeSettings.Level, tas.QRcodeSettings.Size)
    if err != nil {
        log.Printf("Fail to generate png raw data of QRcode: %v\n", err)
        c.AbortWithStatusJSON(http.StatusBadRequest, gin.H {
            "status":  "fail",
            "message": "Fail to generate png raw data of QRcode.",
        })
        return
    }
    _, err = buffer.Write(raw)
    c.Writer.Header().Set("Content-Type", "image/png")
    c.Writer.Header().Set("Content-Length", strconv.Itoa(len(buffer.Bytes())))
    if _, err = c.Writer.Write(buffer.Bytes()); err != nil {
        log.Printf("Unable to write image raw data from buffer: %v\n", err)
        c.Abort()
    }
}

// @Summary Show User Secret Key
// @Description get secret key by username
// @Tags user
// @Accept application/json
// @Produce application/json
// @Security ApiKeyAuth
// @Security ApiSecretAuth
// @Security ApiUserAuth
// @Param name path string true "User Name"
// @Success 200 {string} string "Success"
// @Failure 400 {string} string "Bad request"
// @Failure 401 {string} string "Unauthorized"
// @Failure 403 {string} string "Forbidden"
// @Failure 404 {string} string "Not found"
// @Router /manage/user/{name}/secret [get]
func (tas *TotpApiSettings) showUserKeyHandler(c *gin.Context) {
    name, err := url.QueryUnescape(c.Param("name"))
    if err != nil {
        c.AbortWithStatusJSON(http.StatusBadRequest, gin.H {
            "operation": "fail",
            "message":   fmt.Sprintf("URL Decode to Query String Error: %v", err),
        })
        return
    }
    secret, err := tas.KeyStorage.GetSecretByUsernameCtx(c, name)
    if err != nil {
        log.Printf("Unable to get secret key by username %s: %v\n",name, err)
        c.AbortWithStatusJSON(http.StatusBadRequest, gin.H {
            "status": "fail",
            "message": fmt.Sprintf("Unable to get secret key by username %s.", name),
        })
        return
    }
    c.String(http.StatusOK, secret)
}

// @Summary Create User
// @Description create a new user with random secret
// @Tags user
// @Accept application/json
// @Produce application/json
// @Security ApiKeyAuth
// @Security ApiSecretAuth
// @Security ApiUserAuth
// @Param name path string true "User Name"
// @Param secret path string false "Secret Key"
// @Success 200 {string} string "Success"
// @Failure 400 {string} string "Bad request"
// @Failure 401 {string} string "Unauthorized"
// @Failure 403 {string} string "Forbidden"
// @Failure 404 {string} string "Not found"
// @Router /manage/user/{name} [post]
func (tas *TotpApiSettings) newUserHandler(c *gin.Context) {
    name, err := url.QueryUnescape(c.Param("name"))
    if err != nil {
        c.AbortWithStatusJSON(http.StatusBadRequest, gin.H {
            "operation": "fail",
            "message":   fmt.Sprintf("URL Decode to Query String Error: %v", err),
        })
        return
    }
    secret := c.PostForm("secret")
    err = tas.KeyStorage.CreateUserCtx(c, name, secret)
    if err != nil {
        log.Printf("Unable to create user %s: %v\n",name, err)
        c.AbortWithStatusJSON(http.StatusBadRequest, gin.H {
            "operation": "fail",
            "message": fmt.Sprintf("Unable to create user %s.",name),
        })
        return
    }
    c.JSON(http.StatusOK, gin.H {
        "status": "success",
        "message": fmt.Sprintf("The user %s has been created.",name),
    })
}

// @Summary Update Secret Key
// @Description regenerate a new secret key for user
// @Tags user
// @Accept application/json
// @Produce application/json
// @Security ApiKeyAuth
// @Security ApiSecretAuth
// @Security ApiUserAuth
// @Param name path string true "User Name"
// @Success 200 {string} string "Success"
// @Failure 400 {string} string "Bad request"
// @Failure 401 {string} string "Unauthorized"
// @Failure 403 {string} string "Forbidden"
// @Failure 404 {string} string "Not found"
// @Router /manage/user/{name}/secret [patch]
func (tas *TotpApiSettings) updateSecretKeyHandler(c *gin.Context) {
    name, err := url.QueryUnescape(c.Param("name"))
    if err != nil {
        c.AbortWithStatusJSON(http.StatusBadRequest, gin.H {
            "operation": "fail",
            "message":   fmt.Sprintf("URL Decode to Query String Error: %v", err),
        })
        return
    }
    secret := totp.GenerateSecretKey(16)
    err = tas.KeyStorage.UpdateSecretByUsernameCtx(c, name, secret)
    if err != nil {
        log.Printf("Unable to update secret key of this user %s: %v\n",name, err)
        c.AbortWithStatusJSON(http.StatusBadRequest, gin.H {
            "operation": "fail",
            "message": fmt.Sprintf("Unable to update secret key of this user %s.",name),
        })
        return
    }
    c.JSON(http.StatusOK, gin.H {
        "status": "success",
        "new-secret-key": secret,
    })
}

// @Summary Rename User
// @Description rename user
// @Tags user
// @Accept application/json
// @Produce application/json
// @Security ApiKeyAuth
// @Security ApiSecretAuth
// @Security ApiUserAuth
// @Param name path string true "User Name"
// @Param new path string true "New User Name"
// @Success 200 {string} string "Success"
// @Failure 400 {string} string "Bad request"
// @Failure 401 {string} string "Unauthorized"
// @Failure 403 {string} string "Forbidden"
// @Failure 404 {string} string "Not found"
// @Router /manage/user/{name}/rename/{new} [patch]
func (tas *TotpApiSettings) renameUserHandler(c *gin.Context) {
    name, err := url.QueryUnescape(c.Param("name"))
    if err != nil {
        c.AbortWithStatusJSON(http.StatusBadRequest, gin.H {
            "operation": "fail",
            "message":   fmt.Sprintf("URL Decode to Query String Error: %v", err),
        })
        return
    }
    new, err := url.QueryUnescape(c.Param("new"))
    if err != nil {
        c.AbortWithStatusJSON(http.StatusBadRequest, gin.H {
            "operation": "fail",
            "message":   fmt.Sprintf("URL Decode to Query String Error: %v", err),
        })
        return
    }
    if err = tas.KeyStorage.UpdateUsernameCtx(c, name, new); err != nil {
        c.AbortWithStatusJSON(http.StatusBadRequest, gin.H {
            "operation": "fail",
            "message":   "Fail to update user name.",
        })
        return
    }
    c.JSON(http.StatusOK, gin.H {
        "operation": "success",
        "message":   fmt.Sprintf("User %s has been renamed to %s.", name, new),
    })
}

// @Summary Delete User
// @Description delete an existing user
// @Tags user
// @Accept application/json
// @Produce application/json
// @Security ApiKeyAuth
// @Security ApiSecretAuth
// @Security ApiUserAuth
// @Param name path string true "User Name"
// @Success 200 {string} string "Success"
// @Failure 400 {string} string "Bad request"
// @Failure 401 {string} string "Unauthorized"
// @Failure 403 {string} string "Forbidden"
// @Failure 404 {string} string "Not found"
// @Router /manage/user/{name} [delete]
func (tas *TotpApiSettings) deleteUserHandler(c *gin.Context) {
    name, err := url.QueryUnescape(c.Param("name"))
    if err != nil {
        c.AbortWithStatusJSON(http.StatusBadRequest, gin.H {
            "operation": "fail",
            "message":   fmt.Sprintf("URL Decode to Query String Error: %v", err),
        })
        return
    }
    err = tas.KeyStorage.DeleteUserByNameCtx(c, name)
    if err != nil {
        log.Printf("Unable to delete this user %s: %v\n",name, err)
        c.AbortWithStatusJSON(http.StatusBadRequest, gin.H {
            "status": "fail",
            "message": fmt.Sprintf("Unable to delete this user %s.",name),
        })
        return
    }
    c.JSON(http.StatusOK, gin.H {
        "status": "success",
        "message": fmt.Sprintf("The user %s has been deleted.",name),
    })
}

// @Summary Create API Key For User
// @Description attach an api key for user
// @Tags user
// @Accept multipart/form-data
// @Produce application/json
// @Security ApiKeyAuth
// @Security ApiSecretAuth
// @Security ApiUserAuth
// @Param name path string true "User Name"
// @Param api-key formData string false "API Key"
// @Param api-secret formData string false "API Secret (salt)"
// @Success 200 {string} string "Success"
// @Failure 400 {string} string "Bad request"
// @Failure 401 {string} string "Unauthorized"
// @Failure 403 {string} string "Forbidden"
// @Failure 404 {string} string "Not found"
// @Router /manage/user/{name}/apikey [post]
func (tas *TotpApiSettings) attachApiKeyToUserHandler(c *gin.Context) {
    name, err := url.QueryUnescape(c.Param("name"))
    if err != nil {
        c.AbortWithStatusJSON(http.StatusBadRequest, gin.H {
            "operation": "fail",
            "message":   fmt.Sprintf("URL Decode to Query String Error: %v", err),
        })
        return
    }
    apikey := c.Request.FormValue("api-key")
    apisecret := c.Request.FormValue("api-secret")
    if apikey == "" {
        apikey = keygen.GenerateRandomSalt(16)
    }
    if apisecret == "" {
        apisecret = keygen.GenerateRandomSalt(8)
    }
    hash := keygen.HashPasswordWithSaltBySha("sha512", apikey, apisecret)
    if err := tas.KeyStorage.AttachApiKeyToUserCtx(c, name, hash); err != nil {
        c.AbortWithStatusJSON(http.StatusBadRequest, gin.H {
            "status": "fail",
            "message": fmt.Sprintf("Unable to attach api key to this user %s.",name),
        })
        return
    }
    c.JSON(http.StatusOK, gin.H {
        "status": "success",
        "api-key": apikey,
        "api-secret": apisecret,
        "message": "Copy the api-key and api-secret and save them. You can't get them again.",
    })
}

// @Summary Regenerate API Key For User
// @Description regenerate new api key for user by totp and admin role (rescure)
// @Tags rescure
// @Accept multipart/form-data
// @Produce application/json
// @Param TOTP-PIN-CODE header string true "TOTP Pin Code"
// @Param TOTP-USER header string true "TOTP User"
// @Param name path string true "User Name"
// @Param api-key formData string false "API Key"
// @Param api-secret formData string false "API Secret (salt)"
// @Success 200 {string} string "Success"
// @Failure 400 {string} string "Bad request"
// @Failure 401 {string} string "Unauthorized"
// @Failure 403 {string} string "Forbidden"
// @Failure 404 {string} string "Not found"
// @Router /rescure/apikey/user/{name} [post]
func (tas *TotpApiSettings) regenApiKeyHandler(c *gin.Context) {
    name, err := url.QueryUnescape(c.Param("name"))
    if err != nil {
        c.AbortWithStatusJSON(http.StatusBadRequest, gin.H {
            "operation": "fail",
            "message":   fmt.Sprintf("URL Decode to Query String Error: %v", err),
        })
        return
    }
    apikey := c.Request.FormValue("api-key")
    apisecret := c.Request.FormValue("api-secret")
    if apikey == "" {
        apikey = keygen.GenerateRandomSalt(16)
    }
    if apisecret == "" {
        apisecret = keygen.GenerateRandomSalt(8)
    }
    hash := keygen.HashPasswordWithSaltBySha("sha512", apikey, apisecret)
    if err := tas.KeyStorage.UpdateApiKeyForUserCtx(c, name, hash); err != nil {
        c.AbortWithStatusJSON(http.StatusBadRequest, gin.H {
            "status": "fail",
            "message": fmt.Sprintf("Unable to update api key of this user %s.",name),
        })
        return
    }
    c.JSON(http.StatusOK, gin.H {
        "status": "success",
        "api-key": apikey,
        "api-secret": apisecret,
        "message": "Copy the api-key and api-secret and save them. You can't get them again.",
    })
}

// @Summary Show All Users
// @Description get all users
// @Tags user
// @Accept application/json
// @Produce application/json
// @Security ApiKeyAuth
// @Security ApiSecretAuth
// @Security ApiUserAuth
// @Param limits path string true "Query Limits" default(0)
// @Success 200 {string} string "Success"
// @Failure 400 {string} string "Bad request"
// @Failure 401 {string} string "Unauthorized"
// @Failure 403 {string} string "Forbidden"
// @Failure 404 {string} string "Not found"
// @Router /manage/users/{limits} [get]
func (tas *TotpApiSettings) showAllUsersHandler(c *gin.Context) {
    limits, err := strconv.Atoi(c.Param("limits"))
    if err != nil {
        c.AbortWithStatusJSON(http.StatusBadRequest, gin.H {
            "status":  "fail",
            "message": "The limits parameter must be an integer.",
        })
        return
    }
    list, err := tas.KeyStorage.GetAllUsers(c, limits)
    if err != nil {
        log.Printf("Unable to get all users: %v\n", err)
        c.AbortWithStatusJSON(http.StatusBadRequest, gin.H {
            "status":  "fail",
            "message": "Unable to get all users.",
        })
        return
    }
    data, err := json.Marshal(list)
    if err != nil {
        log.Printf("Fail to parse data as json: %v\n", err)
        c.AbortWithStatusJSON(http.StatusBadRequest, gin.H {
            "status": "fail",
            "message": "Fail to parse data as json.",
        })
        return
    }
    c.String(http.StatusOK, string(data))
}

// @Summary Show All Roles
// @Description get all roles
// @Tags role
// @Accept application/json
// @Produce application/json
// @Security ApiKeyAuth
// @Security ApiSecretAuth
// @Security ApiUserAuth
// @Param limits path string true "Query Limits" default(0)
// @Success 200 {string} string "Success"
// @Failure 400 {string} string "Bad request"
// @Failure 401 {string} string "Unauthorized"
// @Failure 403 {string} string "Forbidden"
// @Failure 404 {string} string "Not found"
// @Router /manage/roles/{limits} [get]
func (tas *TotpApiSettings) showAllRolesHandler(c *gin.Context) {
    limits, err := strconv.Atoi(c.Param("limits"))
    if err != nil {
        c.AbortWithStatusJSON(http.StatusBadRequest, gin.H {
            "status":  "fail",
            "message": "The limits parameter must be an integer.",
        })
        return
    }
    list, err := tas.KeyStorage.GetAllRolesCtx(c, limits)
    if err != nil {
        log.Printf("Unable to get all roles: %v\n", err)
        c.AbortWithStatusJSON(http.StatusBadRequest, gin.H {
            "status":  "fail",
            "message": "Unable to get all roles.",
        })
        return
    }
    data, err := json.Marshal(list)
    if err != nil {
        log.Printf("Fail to parse data as json: %v\n", err)
        c.AbortWithStatusJSON(http.StatusBadRequest, gin.H {
            "status": "fail",
            "message": "Fail to parse data as json.",
        })
        return
    }
    c.String(http.StatusOK, string(data))
}

// @Summary Create Role
// @Description create a new role
// @Tags role
// @Accept application/json
// @Produce application/json
// @Security ApiKeyAuth
// @Security ApiSecretAuth
// @Security ApiUserAuth
// @Param name path string true "Role Name"
// @Success 200 {string} string "Success"
// @Failure 400 {string} string "Bad request"
// @Failure 401 {string} string "Unauthorized"
// @Failure 403 {string} string "Forbidden"
// @Failure 404 {string} string "Not found"
// @Router /manage/role/{name} [post]
func (tas *TotpApiSettings) createRoleHandler(c *gin.Context) {
    name, err := url.QueryUnescape(c.Param("name"))
    if err != nil {
        c.AbortWithStatusJSON(http.StatusBadRequest, gin.H {
            "operation": "fail",
            "message":   fmt.Sprintf("URL Decode to Query String Error: %v", err),
        })
        return
    }
    if err := tas.KeyStorage.CreateRoleCtx(c, name); err != nil {
        log.Printf("Unable to create role %s: %v\n", name, err)
        c.AbortWithStatusJSON(http.StatusBadRequest, gin.H {
            "status": "fail",
            "message": fmt.Sprintf("Fail to create role %s.", name),
        })
        return
    }
    c.JSON(http.StatusOK, gin.H {
        "status": "success",
        "message": fmt.Sprintf("The role %s has been created.", name),
    })
}

// @Summary Delete Role
// @Description delete a role
// @Tags role
// @Accept application/json
// @Produce application/json
// @Security ApiKeyAuth
// @Security ApiSecretAuth
// @Security ApiUserAuth
// @Param name path string true "Role Name"
// @Success 200 {string} string "Success"
// @Failure 400 {string} string "Bad request"
// @Failure 401 {string} string "Unauthorized"
// @Failure 403 {string} string "Forbidden"
// @Failure 404 {string} string "Not found"
// @Router /manage/role/{name} [delete]
func (tas *TotpApiSettings) deleteRoleHandler(c *gin.Context) {
    name, err := url.QueryUnescape(c.Param("name"))
    if err != nil {
        c.AbortWithStatusJSON(http.StatusBadRequest, gin.H {
            "operation": "fail",
            "message":   fmt.Sprintf("URL Decode to Query String Error: %v", err),
        })
        return
    }
    if name == "admin" {
        c.AbortWithStatusJSON(http.StatusBadRequest, gin.H {
            "status": "fail",
            "message": fmt.Sprintf("Unable to delete the default role %s.", name),
        })
        return
    }
    if err := tas.KeyStorage.DeleteRoleByNameCtx(c, name); err != nil {
        log.Printf("Unable to delete role %s: %v\n", name, err)
        c.AbortWithStatusJSON(http.StatusBadRequest, gin.H {
            "status": "fail",
            "message": fmt.Sprintf("Unable to delete role %s.", name),
        })
        return
    }
    c.JSON(http.StatusOK, gin.H {
        "status": "success",
        "message": fmt.Sprintf("The role %s has been deleted.", name),
    })
}

// @Summary Grant Role To User
// @Description grant a role to the user
// @Tags role
// @Accept application/json
// @Produce application/json
// @Security ApiKeyAuth
// @Security ApiSecretAuth
// @Security ApiUserAuth
// @Param user path string true "User Name"
// @Param name path string true "Role Name"
// @Success 200 {string} string "Success"
// @Failure 400 {string} string "Bad request"
// @Failure 401 {string} string "Unauthorized"
// @Failure 403 {string} string "Forbidden"
// @Failure 404 {string} string "Not found"
// @Router /manage/role/{name}/user/{user} [post]
func (tas *TotpApiSettings) grantRoleToUserHandler(c *gin.Context) {
    name, err := url.QueryUnescape(c.Param("name"))
    if err != nil {
        c.AbortWithStatusJSON(http.StatusBadRequest, gin.H {
            "operation": "fail",
            "message":   fmt.Sprintf("URL Decode to Query String Error: %v", err),
        })
        return
    }
    user, err := url.QueryUnescape(c.Param("user"))
    if err != nil {
        c.AbortWithStatusJSON(http.StatusBadRequest, gin.H {
            "operation": "fail",
            "message":   fmt.Sprintf("URL Decode to Query String Error: %v", err),
        })
        return
    }
    if err := tas.KeyStorage.GrantRolesToUserCtx(c, user, name); err != nil {
        log.Printf("Unable to grant role %s to user %s: %v\n", name, user, err)
        c.AbortWithStatusJSON(http.StatusBadRequest, gin.H {
            "status": "fail",
            "message": fmt.Sprintf("Unable to grant role %s to user %s.", name, user),
        })
        return
    }
    c.JSON(http.StatusOK, gin.H {
        "status": "success",
        "message": fmt.Sprintf("The role %s has been granted to user %s.", name, user),
    })
}

// @Summary Delete Role From User
// @Description remove a role from user
// @Tags role
// @Accept application/json
// @Produce application/json
// @Security ApiKeyAuth
// @Security ApiSecretAuth
// @Security ApiUserAuth
// @Param user path string true "User Name"
// @Param name path string true "Role Name"
// @Success 200 {string} string "Success"
// @Failure 400 {string} string "Bad request"
// @Failure 401 {string} string "Unauthorized"
// @Failure 403 {string} string "Forbidden"
// @Failure 404 {string} string "Not found"
// @Router /manage/role/{name}/user/{user} [delete]
func (tas *TotpApiSettings) deleteRoleFromUserHandler(c *gin.Context) {
    name, err := url.QueryUnescape(c.Param("name"))
    if err != nil {
        c.AbortWithStatusJSON(http.StatusBadRequest, gin.H {
            "operation": "fail",
            "message":   fmt.Sprintf("URL Decode to Query String Error: %v", err),
        })
        return
    }
    user, err := url.QueryUnescape(c.Param("user"))
    if err != nil {
        c.AbortWithStatusJSON(http.StatusBadRequest, gin.H {
            "operation": "fail",
            "message":   fmt.Sprintf("URL Decode to Query String Error: %v", err),
        })
        return
    }
    if err := tas.KeyStorage.RemoveRolesFromUserCtx(c, user, name); err != nil {
        log.Printf("Unable to remove role %s from user %s: %v\n", name, user, err)
        c.AbortWithStatusJSON(http.StatusBadRequest, gin.H {
            "status": "fail",
            "message": fmt.Sprintf("Unable to remove role %s from user %s.", name, user),
        })
        return
    }
    c.JSON(http.StatusOK, gin.H {
        "status": "success",
        "message": fmt.Sprintf("The role %s has been removed from user %s.", name, user),
    })
}

// @Summary Check Role For User
// @Description check role for user
// @Tags user
// @Accept application/json
// @Produce application/json
// @Security ApiKeyAuth
// @Security ApiSecretAuth
// @Security ApiUserAuth
// @Param name path string true "User Name"
// @Param role path string true "Role Name"
// @Success 200 {string} string "Success"
// @Failure 400 {string} string "Bad request"
// @Failure 401 {string} string "Unauthorized"
// @Failure 403 {string} string "Forbidden"
// @Failure 404 {string} string "Not found"
// @Router /manage/user/{name}/role/{role} [get]
func (tas *TotpApiSettings) checkRoleForUserKeyHandler(c *gin.Context) {
    user, err := url.QueryUnescape(c.Param("user"))
    if err != nil {
        c.AbortWithStatusJSON(http.StatusBadRequest, gin.H {
            "operation": "fail",
            "message":   fmt.Sprintf("URL Decode to Query String Error: %v", err),
        })
        return
    }
    role, err := url.QueryUnescape(c.Param("role"))
    if err != nil {
        c.AbortWithStatusJSON(http.StatusBadRequest, gin.H {
            "operation": "fail",
            "message":   fmt.Sprintf("URL Decode to Query String Error: %v", err),
        })
        return
    }
    check, err := tas.KeyStorage.CheckRoleForUserCtx(c, user, role)
    if err != nil {
        log.Printf("Unable to check role %s for user %s: %v\n", role, user, err)
        c.AbortWithStatusJSON(http.StatusBadRequest, gin.H {
            "status": "fail",
            "message": fmt.Sprintf("Unable to check role %s for user %s.", role, user),
        })
        return
    }
    c.JSON(http.StatusOK, gin.H {
        "status": "success",
        "result": check,
    })
}

// @Summary Show Roles Of User
// @Description show roles of user
// @Tags user
// @Accept application/json
// @Produce application/json
// @Security ApiKeyAuth
// @Security ApiSecretAuth
// @Security ApiUserAuth
// @Param name path string true "User Name"
// @Success 200 {string} string "Success"
// @Failure 400 {string} string "Bad request"
// @Failure 401 {string} string "Unauthorized"
// @Failure 403 {string} string "Forbidden"
// @Failure 404 {string} string "Not found"
// @Router /manage/user/{name}/roles [get]
func (tas *TotpApiSettings) showRolesOfUserKeyHandler(c *gin.Context) {
    name, err := url.QueryUnescape(c.Param("name"))
    if err != nil {
        c.AbortWithStatusJSON(http.StatusBadRequest, gin.H {
            "operation": "fail",
            "message":   fmt.Sprintf("URL Decode to Query String Error: %v", err),
        })
        return
    }
    list ,err := tas.KeyStorage.GetRolesBelongToUserCtx(c, name)
    if err != nil {
        log.Printf("Unable to get roles from user %s: %v\n", name, err)
        c.AbortWithStatusJSON(http.StatusBadRequest, gin.H {
            "status": "fail",
            "message": fmt.Sprintf("Unable to get roles from user %s.", name),
        })
        return
    }
    data, err := json.Marshal(list)
    if err != nil {
        log.Printf("Fail to parse data as json: %v\n", err)
        c.AbortWithStatusJSON(http.StatusBadRequest, gin.H {
            "status": "fail",
            "message": "Fail to parse data as json.",
        })
        return
    }
    c.String(http.StatusOK, string(data))
}

// @Summary Verify TOTP Pin Code
// @Description verify totp pin code and return username, roles if passed
// @Tags auth
// @Accept application/json
// @Produce application/json
// @Param TOTP-PIN-CODE header string true "Pin Code"
// @Param TOTP-USER header string true "User Name"
// @Success 200 {string} string "Success"
// @Failure 400 {string} string "Bad request"
// @Failure 401 {string} string "Unauthorized"
// @Failure 403 {string} string "Forbidden"
// @Failure 404 {string} string "Not found"
// @Router /auth/verify [get]
func (tas *TotpApiSettings) verifyTotpPinCodeHandler(c *gin.Context) {
    pincode := c.GetHeader("TOTP-PIN-CODE")
    username := c.GetHeader("TOTP-USER")
    if len(pincode) == 0 || username == "" {
        c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H {
            "validation": false,
            "message": "The TOTP-PIN-CODE and TOTP-USER headers are both required.",
        })
        return
    }
    secret, err := tas.KeyStorage.GetSecretByUsernameCtx(c, username)
    if err != nil {
        log.Printf("Unable to get secret key by username %s: %v\n",username, err)
        c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H {
            "validation": false,
            "message": fmt.Sprintf("Unable to get secret key by username %s.",username),
        })
        return
    }
    list, err := tas.KeyStorage.GetRolesBelongToUserCtx(c, username)
    if err != nil {
        log.Printf("Unable to get roles by username %s: %v\n",username, err)
        c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H {
            "validation": false,
            "message": fmt.Sprintf("Unable to get roles by username %s.",username),
        })
        return
    }
    var verify bool = false
    if tas.Controller.TotpProperies.Tolerance {
        verify = totp.MatchTolerantTOTPCtx(
            c,
            secret,
            tas.Controller.TotpProperies.TimeStep,
            tas.Controller.TotpProperies.ShiftSeconds,
            tas.Controller.TotpProperies.Digits,
            tas.Controller.TotpProperies.TolerantSteps,
            tas.Controller.TotpProperies.Algorithm,
            pincode,
        )
    } else {
        verify = totp.MatchTOTP(
            secret,
            tas.Controller.TotpProperies.TimeStep,
            tas.Controller.TotpProperies.Digits,
            tas.Controller.TotpProperies.Algorithm,
            pincode,
        )
    }
    if ! verify {
        c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H {
            "validation": false,
            "username": username,
            "roles": list,
        })
        return
    }
    c.JSON(http.StatusOK, gin.H {
        "validation": true,
        "username": username,
        "roles": list,
    })
}
