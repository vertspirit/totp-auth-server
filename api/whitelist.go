package api

import (
    "fmt"
    "log"
    "net/http"
    "encoding/json"
    "strconv"
    "github.com/gin-gonic/gin"

    "totp-auth-server/pkg/totp"
)

// @Summary Show White List
// @Description show current enabled white list
// @Tags whitelist
// @Accept application/json
// @Produce application/json
// @Security ApiKeyAuth
// @Security ApiSecretAuth
// @Security ApiUserAuth
// @Success 200 {string} string "Success"
// @Failure 400 {string} string "Unable to show current enabled white list"
// @Failure 401 {string} string "Unauthorized"
// @Failure 403 {string} string "Forbidden"
// @Failure 404 {string} string "Not found"
// @Router /manage/whitelist [get]
func (tas *TotpApiSettings) showWhitelistHandler(c *gin.Context) {
    list, err := tas.KeyStorage.GetWhitelistCtx(c)
    if err != nil {
        c.AbortWithStatusJSON(http.StatusBadRequest, gin.H {
            "status":  "fail",
            "message": "Unable to get whitelist.",
        })
        return
    }
    data, err := json.Marshal(list)
    if err != nil {
        log.Printf("Parse Json Error: %v\n", err)
        c.AbortWithStatusJSON(http.StatusBadRequest, gin.H {
            "status": "success",
            "message": "Fail to parse data as json.",
        })
        return
    }
    c.String(http.StatusOK, string(data))
}

// @Summary Add CIDR to White List
// @Description add ipv4 cidr to white list
// @Tags whitelist
// @Accept application/json
// @Produce application/json
// @Security ApiKeyAuth
// @Security ApiSecretAuth
// @Security ApiUserAuth
// @Param addr path string true "IP Address"
// @Param cidr path string true "CIDR Bit Count"
// @Success 200 {string} string "Success"
// @Failure 400 {string} string "Unable to add ipv4 cidr to white list"
// @Failure 401 {string} string "Unauthorized"
// @Failure 403 {string} string "Forbidden"
// @Failure 404 {string} string "Not found"
// @Router /manage/whitelist/{addr}/{cidr} [post]
func (tas *TotpApiSettings) addCidrToWhitelistHandler(c *gin.Context) {
    addr := c.Param("addr")
    cidr, err := strconv.Atoi(c.Param("cidr"))
    if err != nil {
        log.Printf("String to Int Error: %v\n", err)
    }
    ic := &totp.IPv4CIDR {
        IPAddress: addr,
        CIDR: cidr,
    }
    err = tas.KeyStorage.AddCidrToWhitelistCtx(c, ic)
    if err != nil {
        c.AbortWithStatusJSON(http.StatusBadRequest, gin.H {
            "status": "fail",
            "message": "Fail to add ip cidr to whitelist.",
        })
        return
    }
    c.JSON(http.StatusOK, gin.H {
        "status": "success",
        "message": fmt.Sprintf("The cidr %s/%d added to whitelist.", addr, cidr),
    })
}

// @Summary Delete CIDR from White List
// @Description delete ipv4 cidr from white list
// @Tags whitelist
// @Accept application/json
// @Produce application/json
// @Security ApiKeyAuth
// @Security ApiSecretAuth
// @Security ApiUserAuth
// @Param addr path string true "IP Address"
// @Param cidr path string true "CIDR Bit Count"
// @Success 200 {string} string "Success"
// @Failure 400 {string} string "Unable to delete ipv4 cidr from white list"
// @Failure 401 {string} string "Unauthorized"
// @Failure 403 {string} string "Forbidden"
// @Failure 404 {string} string "Not found"
// @Router /manage/whitelist/{addr}/{cidr} [delete]
func (tas *TotpApiSettings) deleteCidrFromWhitelistHandler(c *gin.Context) {
    addr := c.Param("addr")
    cidr, err := strconv.Atoi(c.Param("cidr"))
    if err != nil {
        log.Printf("String to Int Error: %v\n", err)
    }
    ic := &totp.IPv4CIDR {
        IPAddress: addr,
        CIDR: cidr,
    }
    err = tas.KeyStorage.DeleteCidrFromWhitelistCtx(c, ic)
    if err != nil {
        c.AbortWithStatusJSON(http.StatusBadRequest, gin.H {
            "status": "fail",
            "message": "Fail to delete ip cidr from whitelist.",
        })
        return
    }
    c.JSON(http.StatusOK, gin.H {
        "status": "success",
        "message": fmt.Sprintf("The cidr %s/%d deleted from whitelist.", addr, cidr),
    })
}

// @Summary Disable CIDR in White List
// @Description disable ipv4 cidr in white list
// @Tags whitelist
// @Accept application/json
// @Produce application/json
// @Security ApiKeyAuth
// @Security ApiSecretAuth
// @Security ApiUserAuth
// @Param addr path string true "IP Address"
// @Param cidr path string true "CIDR Bit Count"
// @Success 200 {string} string "Success"
// @Failure 400 {string} string "Unable to disable ipv4 cidr in white list"
// @Failure 401 {string} string "Unauthorized"
// @Failure 403 {string} string "Forbidden"
// @Failure 404 {string} string "Not found"
// @Router /manage/whitelist/disable/{addr}/{cidr} [post]
func (tas *TotpApiSettings) disableCidrInWhitelistHandler(c *gin.Context) {
    addr := c.Param("addr")
    cidr, err := strconv.Atoi(c.Param("cidr"))
    if err != nil {
        log.Printf("String to Int Error: %v\n", err)
    }
    ic := &totp.IPv4CIDR {
        IPAddress: addr,
        CIDR: cidr,
    }
    err = tas.KeyStorage.ToggleCidr(c, ic, 0)
    if err != nil {
        c.AbortWithStatusJSON(http.StatusBadRequest, gin.H {
            "status": "fail",
            "message": "Fail to disable ip cidr in whitelist.",
        })
        return
    }
    c.JSON(http.StatusOK, gin.H {
        "status": "success",
        "message": fmt.Sprintf("The cidr %s/%d disabled.", addr, cidr),
    })
}

// @Summary Enable CIDR in White List
// @Description enable ipv4 cidr in white list
// @Tags whitelist
// @Accept application/json
// @Produce application/json
// @Security ApiKeyAuth
// @Security ApiSecretAuth
// @Security ApiUserAuth
// @Param addr path string true "IP Address"
// @Param cidr path string true "CIDR Bit Count"
// @Success 200 {string} string "Success"
// @Failure 400 {string} string "Unable to enable ipv4 cidr in white list"
// @Failure 401 {string} string "Unauthorized"
// @Failure 403 {string} string "Forbidden"
// @Failure 404 {string} string "Not found"
// @Router /manage/whitelist/enable/{addr}/{cidr} [post]
func (tas *TotpApiSettings) enableCidrInWhitelistHandler(c *gin.Context) {
    addr := c.Param("addr")
    cidr, err := strconv.Atoi(c.Param("cidr"))
    if err != nil {
        log.Printf("String to Int Error: %v\n", err)
    }
    ic := &totp.IPv4CIDR {
        IPAddress: addr,
        CIDR: cidr,
    }
    err = tas.KeyStorage.ToggleCidr(c, ic, 1)
    if err != nil {
        c.AbortWithStatusJSON(http.StatusBadRequest, gin.H {
            "status": "fail",
            "message": "Fail to enable ip cidr in whitelist.",
        })
        return
    }
    c.JSON(http.StatusOK, gin.H {
        "status": "success",
        "message": fmt.Sprintf("The cidr %s/%d enabled.", addr, cidr),
    })
}
