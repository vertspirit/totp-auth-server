package api

import (
    "strings"
    "os"
    "os/signal"
    "syscall"
    "time"
    "context"
    "net/http"
    "log"
    "github.com/gin-gonic/gin"
    ma "github.com/aviddiviner/gin-limit"
    al "github.com/bu/gin-access-limit"
    "github.com/gin-contrib/pprof"
    "github.com/fvbock/endless"

    "totp-auth-server/pkg/totp"
    docs "totp-auth-server/docs"
)

type TotpApiSettings struct {
    Version          string
    SubpathPrefix    string
    Controller       *totp.Controller
    KeyStorage       *totp.MysqlKeyStorage
    ApiKeyAlgorithm  string
    RealIpHeader     string
    AdminRoles       []string
    Whitelist        []string
    QRcodeSettings   *totp.QRcodeConfig
    MaxAllowedConns  int
    ReadTimeout      int
    WriteTimeout     int
    MaxHeaderBytes   int
    EnablePprof      bool
}

func (tas * TotpApiSettings) SetupRouter(swagHdler gin.HandlerFunc) *gin.Engine {
    router := gin.New()
    router.Use(gin.Logger())
    if tas.EnablePprof {
        pprof.Register(router, "debug/pprof")
    }
    router.SetTrustedProxies(nil)
    router.RemoteIPHeaders = []string{
        "X-Forwarded-For",
        "X-Real-IP",
    }
    if tas.RealIpHeader == "" {
        tas.RealIpHeader = "X-Forwarded-For"
    }
    al.TrustedHeaderField = tas.RealIpHeader

    whitelist := strings.Join(tas.Whitelist[:], ",")
    if whitelist == "" {
        whitelist = "0.0.0.0/0"
    }

    if tas.MaxAllowedConns != 0 {
        router.Use(ma.MaxAllowed(tas.MaxAllowedConns))
    }

    if swagHdler != nil {
        if tas.SubpathPrefix != "" {
            docs.SwaggerInfo.BasePath = tas.SubpathPrefix
        } else {
            docs.SwaggerInfo.BasePath = "/"
        }
        router.GET("/swagger/*any", swagHdler)
    }

    router.GET("/info", tas.showInfoHandler)
    router.GET("/keygen", tas.generateKeyHandler)
    router.POST("/rescure/apikey/user/:name", tas.authByTotpWithRole(), tas.regenApiKeyHandler)

    authRouter := router.Group("/auth")
    authRouter.GET("/verify", tas.verifyTotpPinCodeHandler)

    manageRouter := router.Group("/manage")
    manageRouter.Use(al.CIDR(whitelist))
    manageRouter.Use(tas.authRequired())
    manageRouter.GET("/users/:limits", tas.showAllUsersHandler)
    manageRouter.GET("/user/:name/qrcode", tas.showUserKeyQRcodeHandler)
    manageRouter.GET("/user/:name/secret", tas.showUserKeyHandler)
    manageRouter.GET("/user/:name/role/:role", tas.checkRoleForUserKeyHandler)
    manageRouter.GET("/user/:name/roles", tas.showRolesOfUserKeyHandler)
    manageRouter.POST("/user/:name", tas.newUserHandler)
    manageRouter.POST("/user/:name/apikey", tas.attachApiKeyToUserHandler)
    manageRouter.PATCH("/user/:name/secret", tas.updateSecretKeyHandler)
    manageRouter.PATCH("/user/:name/rename/:new", tas.renameUserHandler)
    manageRouter.DELETE("/user/:name", tas.deleteUserHandler)

    manageRouter.GET("/roles/:limits", tas.showAllRolesHandler)
    manageRouter.POST("/role/:name", tas.createRoleHandler)
    manageRouter.POST("/role/:name/user/:user", tas.grantRoleToUserHandler)
    manageRouter.DELETE("/role/:name", tas.deleteRoleHandler)
    manageRouter.DELETE("/role/:name/user/:user", tas.deleteRoleFromUserHandler)

    manageRouter.GET("/whitelist", tas.showWhitelistHandler)
    manageRouter.POST("/whitelist/:addr/:cidr", tas.addCidrToWhitelistHandler)
    manageRouter.POST("/whitelist/disable/:addr/:cidr", tas.disableCidrInWhitelistHandler)
    manageRouter.POST("/whitelist/enable/:addr/:cidr", tas.enableCidrInWhitelistHandler)
    manageRouter.DELETE("/whitelist/:addr/:cidr", tas.deleteCidrFromWhitelistHandler)

    return router
}

func (tas * TotpApiSettings) StartupServer(addr string, swhd gin.HandlerFunc, gf bool) {
    router := tas.SetupRouter(swhd)
    if gf {
        endless.DefaultReadTimeOut = time.Duration(tas.ReadTimeout) * time.Second
        endless.DefaultWriteTimeOut = time.Duration(tas.WriteTimeout) * time.Second
        endless.DefaultMaxHeaderBytes = tas.MaxHeaderBytes << 20 // default is max 2 MB
        srv := endless.NewServer(addr, router)
        srv.BeforeBegin = func(add string){
            log.Printf("Actual PID: %d\n", syscall.Getpid())
        }
        err := srv.ListenAndServe()
        if err != nil {
            log.Printf("Server Error: %v\n", err)
        }
    } else {
        srv := &http.Server {
            Addr:           addr,
            Handler:        router,
            ReadTimeout:    time.Duration(tas.ReadTimeout) * time.Second,
            WriteTimeout:   time.Duration(tas.WriteTimeout) * time.Second,
            MaxHeaderBytes: tas.MaxHeaderBytes << 20,
        }
        go func() {
            if err := srv.ListenAndServe(); err != nil {
                log.Printf("Server Listen Error: %s\n", err)
            }
        }()
        quit := make(chan os.Signal, 1)
        signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
        <-quit
        log.Println("Shutting down server...")
        ctx, cancel := context.WithTimeout(context.Background(), 5 * time.Second)
        defer cancel()
        if err := srv.Shutdown(ctx); err != nil {
            log.Fatal("Server forced to shutdown:", err)
        }
    }
}
