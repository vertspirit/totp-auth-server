package api

import(
    "net/http"
    "fmt"
    "log"
    "github.com/gin-gonic/gin"

    "totp-auth-server/pkg/keygen"
    "totp-auth-server/pkg/totp"
)

// Middleware: api-key authentication for management
func (tas * TotpApiSettings) authRequired() gin.HandlerFunc {
    return func(c *gin.Context) {
        authAPIKeyHeader := c.GetHeader("X-API-Key")
        authSecretHeader := c.GetHeader("X-Secret-Key")
        username := c.GetHeader("User-Name")

        if len(authAPIKeyHeader) == 0 || len(authSecretHeader) == 0 {
            c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H {
                "message": "X-API-Key and X-Secret-Key are required Header for the authorization.",
            })
            return
        }
        if len(username) == 0 {
            c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H {
                "message": "User-Name are required Header for the authorization.",
            })
            return
        }
        if tas.AdminRoles == nil {
            tas.AdminRoles = []string{"admin"}
        }
        var (
            matched bool = false
            err     error
        )
        for _, rl := range tas.AdminRoles {
            matched, err = tas.KeyStorage.CheckRoleForUserCtx(c, username, rl)
            if err != nil {
                log.Printf("Fail to chekc role %s for this user %s: %v\n", rl, username, err)
                c.AbortWithStatusJSON(http.StatusBadRequest, gin.H {
                    "message": fmt.Sprintf("Fail to chekc role %s for this user %s.", rl, username),
                })
                return
            }
            if matched {
                break
            }
        }
        if ! matched {
            c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H {
                "message": fmt.Sprintf("This user %s has no permission.", username),
            })
            return
        }
        keyHashs, err := tas.KeyStorage.GetUserApiKeysCtx(c, username)
        if err != nil {
            log.Printf("Fail to get api-keys of this user %s: %v\n", username, err)
            c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H {
                "message": fmt.Sprintf("Fail to get api-keys of this user %s.", username),
            })
        }
        if tas.ApiKeyAlgorithm == "" {
            tas.ApiKeyAlgorithm = "sha512"
        }
        matched = false
        for _, hash := range keyHashs {
            if keygen.MatchPassword(tas.ApiKeyAlgorithm , hash, authAPIKeyHeader, authSecretHeader) {
                matched = true
                break
            }
        }
        if ! matched {
            c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H {
                "message": fmt.Sprintf(
                    "This user %s isn't authorized for this operation: X-API-Key = %s, X-Secret-Key = %s",
                    username,
                    authAPIKeyHeader,
                    authSecretHeader,
                ),
            })
            return
        }
        c.Next()
    }
}

func (tas * TotpApiSettings) authByTotpWithRole() gin.HandlerFunc {
    return func(c *gin.Context) {
        pincode := c.GetHeader("TOTP-PIN-CODE")
        username := c.GetHeader("TOTP-USER")
        if len(pincode) == 0 || username == "" {
            c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H {
                "validation": false,
                "message": "The TOTP-PIN-CODE and TOTP-USER headers are both required.",
            })
            return
        }
        secret, err := tas.KeyStorage.GetSecretByUsernameCtx(c, username)
        if err != nil {
            log.Printf("Unable to get secret key by username %s: %v\n",username, err)
            c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H {
                "validation": false,
                "message": fmt.Sprintf("Unable to get secret key by username %s.",username),
            })
            return
        }
        roles, err := tas.KeyStorage.GetRolesBelongToUserCtx(c, username)
        if err != nil {
            log.Printf("Fail to get roles belong to this user %s: %v\n", username, err)
            c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H {
                "message": fmt.Sprintf("Fail to get roles belong to this user %s.", username),
            })
            return
        }
        var matched bool = false
        for _, rl := range roles {
            if rl == "admin" {
                matched = true
                break
            }
        }
        if ! matched {
            c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H {
                "message": fmt.Sprintf("This user %s has no permission.", username),
            })
            return
        }
        var verify bool = false
        if tas.Controller.TotpProperies.Tolerance {
            verify = totp.MatchTolerantTOTPCtx(
                c,
                secret,
                tas.Controller.TotpProperies.TimeStep,
                tas.Controller.TotpProperies.ShiftSeconds,
                tas.Controller.TotpProperies.Digits,
                tas.Controller.TotpProperies.TolerantSteps,
                tas.Controller.TotpProperies.Algorithm,
                pincode,
            )
        } else {
            verify = totp.MatchTOTP(
                secret,
                tas.Controller.TotpProperies.TimeStep,
                tas.Controller.TotpProperies.Digits,
                tas.Controller.TotpProperies.Algorithm,
                pincode,
            )
        }
        if ! verify {
            c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H {
                "validation": false,
                "username": username,
                "roles": roles,
            })
            return
        }
        c.Next()
    }
}
