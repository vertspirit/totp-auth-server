package keygen

import (
    "fmt"
    "hash"
    "crypto/sha1"
    "crypto/sha256"
    "crypto/sha512"
    "crypto/rand"
    "encoding/base64"
    "encoding/hex"
)

func GenerateRandomSalt(length int) string {
    // the length is bytes of salt string
    salt := make([]byte, length)

    // fills the elements of a specified array of bytes with random numbers
    _, err := rand.Read(salt)
    if err != nil {
        fmt.Printf("Fail to generate a salt with random numbers: %s\n", err)
    }

    return hex.EncodeToString(salt)
}

func HashPasswordWithSaltBySha(alg, password ,salt string) string {
    saltedPassword := append([]byte(password), []byte(salt)...)
    var h hash.Hash
    switch alg {
    case "sha1", "sha128":
        h = sha1.New()
    case "sha512":
        h = sha512.New()
    default:
        h = sha256.New()
    }
    h.Write(saltedPassword)
    sum := h.Sum(nil)

    return base64.URLEncoding.EncodeToString(sum)
}

func MatchPassword(alg, hash, password, salt string) bool {
    hashedPassword := HashPasswordWithSaltBySha(alg, password, salt)
    return hashedPassword == hash
}
