package keygen

import (
    "github.com/google/uuid"
)

func GenerateUUID(version string, noDash bool) (string, string, error) {
    /// version could be V1, V2DCEGroup ,V2DCEPerson, V3, V4, V5, and defualt is V4
    var (
        id   uuid.UUID
        err  error
    )
    switch version {
    case "V1":
        if id, err = uuid.NewUUID(); err != nil {
            return id.Version().String(), "", err
        }
    case "V2DCEGroup":
        if id, err = uuid.NewDCEGroup(); err != nil {
          return id.Version().String(), "", err
        }
    case "V2DCEPerson":
        if id, err = uuid.NewDCEPerson(); err != nil {
            return id.Version().String(), "", err
        }
    case "V3":
        if id, err = uuid.NewDCEPerson(); err != nil {
            return id.Version().String(), "", err
        }
    case "V5":
        uid , err := uuid.NewDCEPerson()
        if err != nil {
            return id.Version().String(), "", err
        }
        id = uuid.NewSHA1(uid, []byte("fssda32"))
    default:
        id = uuid.New()
    }
    return id.Version().String(), id.String(), nil
}
