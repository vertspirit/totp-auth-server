package totp

type TotpProperties struct {
    Algorithm     string  `yaml:"algorithm"` // sha1, sha256, sha512
    TimeStep      int64   `yaml:"timestep"`
    Digits        int     `yaml:"digits"`
    Tolerance     bool    `yaml:"tolerance_mode"`
    TolerantSteps int     `yaml:"tolerant_steps"`
    ShiftSeconds  int64   `yaml:"shift_seconds"`
}

type QRcodeConfig struct {
    Level  string  `deafult:"medium"`
    Size   int     `deafult:"512"`
}

type IPv4CIDR struct {
    IPAddress string
    CIDR      int
}

type UserModel struct {
    Username   string
    SecretKey  string
}

func (um *UserModel) GetName() string {
    return um.Username
}

func (um *UserModel) GetKey() string {
    return um.SecretKey
}

func (tp *TotpProperties) GetAlgorithm() string {
    return tp.Algorithm
}

func (tp *TotpProperties) GetTimeStep() int64 {
    return tp.TimeStep
}

func (tp *TotpProperties) GetDigits() int {
    return tp.Digits
}
