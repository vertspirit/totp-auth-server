package totp

import (
    "fmt"
    "encoding/base32"
    "encoding/binary"
    "hash"
    "crypto/hmac"
    "crypto/sha1"
    "crypto/sha256"
    "crypto/sha512"
    "strings"
    "time"
    "strconv"
    "bytes"
    "math/rand"
    "unsafe"
    "image/color"
    qrcode "github.com/skip2/go-qrcode"
    "context"
    "golang.org/x/sync/errgroup"
)

func CreateQRCodePng(data , level, filename string, pixel int) {
    // the default RecoveryLevel of QRCode is medium
    var qrlv qrcode.RecoveryLevel
    switch level {
    case "low":
        qrlv = qrcode.Low
    case "high":
        qrlv = qrcode.High
    case "Highest":
        qrlv = qrcode.Highest
    default:
        qrlv = qrcode.Medium
    }
    if filename == "" {
        filename = "qr.png"
    }
    if pixel == 0 {
        pixel = 256
    }
    err := qrcode.WriteColorFile(data, qrlv, pixel, color.White, color.Black, filename)
    if err != nil {
        fmt.Println(err)
    }
}

func CreateQRCodePngRaw(data , level string, size int) ([]byte, error) {
    var qrlv qrcode.RecoveryLevel
    raw := make([]byte, 0)
    switch level {
    case "low":
        qrlv = qrcode.Low
    case "high":
        qrlv = qrcode.High
    case "Highest":
        qrlv = qrcode.Highest
    default:
        qrlv = qrcode.Medium
    }
    if size == 0 {
        size = 256
    }
    raw, err := qrcode.Encode(data, qrlv, size)
    if err != nil {
        return raw, err
    }
    return raw, nil
}

func GenerateSecretKey(length int) string {
    // The RFC 4648 Base 32 alphabet only allowed [A-Z] and [2-7]
    digits := "234567"
    uppercase := "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    char := digits + uppercase

    rand.Seed(time.Now().UnixNano())
    key := make([]byte, length)
    for i := 0; i < length; i++ {
        key[i] = char[rand.Intn(len(char))]
    }

    return *(*string)(unsafe.Pointer(&key))
}

func EncodeBase32(key string) string {
    secretKey := strings.ToUpper(key)
    keyb32, err := base32.StdEncoding.DecodeString(secretKey)
    if err != nil {
        fmt.Println("Fail to encoding by base32:", err)
        return "error"
    }
    return fmt.Sprintf("%x", keyb32)
}

func GenerateHOTP(key string, count int64, length int, alg string) string {
    // Convert the secret key to base32 encoded string
    secretKey := strings.ToUpper(key)
    keyB32Encoded, err := base32.StdEncoding.DecodeString(secretKey)
    if err != nil {
        fmt.Println("Fail to encoding by base32:", err)
    }

    // Create an arry of 8 bytes and write the count to the []byte array
    buf := make([]byte, 8)
    binary.BigEndian.PutUint64(buf, uint64(count))

    // hash the secret key with count (timeStep) by HMAC-SHA1 Algorithm
    var mac hash.Hash
    switch alg {
    case "sha256":
        mac = hmac.New(sha256.New, keyB32Encoded)
    case "sha512":
        mac = hmac.New(sha512.New, keyB32Encoded)
    default:
        mac = hmac.New(sha1.New, keyB32Encoded)
    }
    mac.Write(buf)
    sum := mac.Sum(nil)

    // Use the last nibble (half-byte, 4bit) as index start from (subset of the generated hash)
    // This number is always appropriate as it's maximum decimal 15
    // SHA1 hash is 20 bytes (maximum index 19) or use len(sum)-1 to calculate, needs 4 bytes (32bit)
    index := (sum[len(sum)-1] & 0x0F)

    // get the 32bit from hash start at index
    var h uint32
    reader := bytes.NewReader(sum[index:index+4])
    if err := binary.Read(reader, binary.BigEndian, &h); err != nil {
        fmt.Println(err)
    }

    // remove most significant bits and generate a remainder less than < 7 digits
    var digits int = 10
    for i := 1; i < length; i++ {
        digits = 10 * digits
    }
    otp := (int(h) & 0x7fffffff) % digits
    otpToken := strconv.Itoa(int(otp))
    if len(otpToken) < length {
        otpToken = "0" + otpToken
    }

    return otpToken
}

func GenerateTOTP(key string, timeStep int64, length int, alg string) string {
    // The TOTP token = HOTP token seeded with every N seconds
    ts := time.Now().Unix() / timeStep
    return GenerateHOTP(key, ts, length, alg)
}

func GenerateTolerantTOTP(key string, step, shift, retry int64, length int, alg string) (string, error) {
    if shift >= step {
        return "", fmt.Errorf("The time shift %d can't be larger than time step %d.\n", shift, step)
    }
    now := time.Now()
    tlt := now.Add(- ( time.Duration(step * retry - shift) * time.Second))
    ts := tlt.Unix() / step
    return GenerateHOTP(key, ts, length, alg), nil
}

func MatchTOTP(key string, timeStep int64, length int, alg string, token string) bool {
    verifyTOTP := GenerateTOTP(key, timeStep, length, alg)
    if verifyTOTP != token {
        return false
    }
    return true
}

func MatchTolerantTOTP(key string, step, shift int64, length, times int, alg string, token string) bool {
    var err error
    calculated := GenerateTOTP(key, step, length, alg)
    if calculated == token {
        return true
    }
    if times == 0 { times = 1 }
    for count := 1; count <= times; count++ {
        calculated, err = GenerateTolerantTOTP(key, step, shift, int64(count), length, alg)
        if err != nil {
            return false
        }
        if calculated == token {
            return true
        }
    }
    return false
}

func MatchTolerantTOTPCtx(ctx context.Context, key string, step, shift int64, length, times int,
    alg string, token string) bool {
    var (
        err      error
        matched  bool = false
    )
    eg, ctx := errgroup.WithContext(ctx)
    calculated := GenerateTOTP(key, step, length, alg)
    if calculated == token {
        return true
    }
    if times == 0 { times = 1 }
    for count := 1; count <= times; count++ {
        cnt := count
        eg.Go(func() error {
            for {
                select {
                case <- ctx.Done():
                    return ctx.Err()
                default:
                    calculated, err = GenerateTolerantTOTP(key, step, shift, int64(cnt), length, alg)
                    if err != nil {
                        return err
                    }
                    if calculated == token {
                        matched = true
                    }
                    return nil
                }
            }
        })
    }
    if err := eg.Wait(); err != nil {
        return false
    }
    return matched
}

func CreateTotpAuthUrl(label, secret, alg string, timeStep int64, length, count int) string {
    url := fmt.Sprintf(
        "otpauth://totp/%s?secret=%s&algorithm=%s&digits=%d&period=%d&count=%d",
        label,
        secret,
        strings.ToUpper(alg),
        length,
        timeStep,
        count,
    )
    return url
}
