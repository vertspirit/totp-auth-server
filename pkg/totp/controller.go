package totp

type Controller struct {
    TotpProperies  *TotpProperties
    KeyStorage     KeyStorage
}

func (ctl *Controller) MapKeyStorage(store KeyStorage) {
    ctl.KeyStorage = store
}
