package totp

import (
    "context"
    "fmt"
    "time"
    "log"
    "database/sql"
    _ "github.com/go-sql-driver/mysql"
)

type KeyStorage interface {
    GetSecretByUsernameCtx(ctx context.Context, name string) (string, error)
    GetRolesBelongToUserCtx(ctx context.Context, name string)  ([]string, error)
    GetUserApiKeysCtx(ctx context.Context, name string) ([]string, error)
}

// Set(&MysqlKeyStorage{}, "default")
type MysqlKeyStorage struct {
    Conn             *sql.DB
    Host             string
    Port             int     `deafult:"3306"`
    Protocol         string  `deafult:"tcp"`
    User             string  `deafult:"totp-auth"`
    Password         string
    Database         string  `deafult:"totp_auth"`
    Charset          string  `deafult:"utf8mb4"`
    StoragEngine     string  `default:"InnoDB"`
    ConnMaxLifetime  int     `default:"60"`
    MaxOpenConns     int     `default:"50"`
    MaxIdleConns     int     `defalut:"20"`
}

func (ms *MysqlKeyStorage) CreateConnection() {
    if ms.Protocol == "" {
        ms.Protocol = "tcp"
    }
    // username:password@protocol(address)/dbname?param=value
    dsn := fmt.Sprintf(
        "%s:%s@%s(%s:%d)/",
        ms.User,
        ms.Password,
        ms.Protocol,
        ms.Host,
        ms.Port,
    )
    db , err := sql.Open("mysql", dsn)
    if err != nil {
        log.Fatal(err)
    }
    ms.Conn = db
    ms.Conn.SetConnMaxLifetime(time.Duration(ms.ConnMaxLifetime) * time.Minute)
    ms.Conn.SetMaxOpenConns(ms.MaxOpenConns)
    ms.Conn.SetMaxIdleConns(ms.MaxIdleConns)
}

func (ms *MysqlKeyStorage) CloseConnection() {
    ms.Conn.Close()
}

func (ms *MysqlKeyStorage) CreateInitDatabase() error {
    stmt := fmt.Sprintf(
        "CREATE DATABASE IF NOT EXISTS %s CHARACTER SET = %s;",
        ms.Database,
        ms.Charset,
    )
    if _, err := ms.Conn.Exec(stmt); err != nil {
        log.Printf("Unable to create initial database: %v\n", err)
        return err
    }
    return nil
}

func (ms *MysqlKeyStorage) CreateInitTables() error {
    if ms.StoragEngine == "" {
        ms.StoragEngine = "InnoDB"
    }
    if ms.Charset == "" {
        ms.Charset = "utf8mb4"
    }
    // tables: users, roles, user_roles, user_apikeys
    stmt := fmt.Sprintf(`
        CREATE TABLE IF NOT EXISTS %s.users (
            user_id     BINARY(16)    NOT NULL,
            user_name   VARCHAR(64)   NOT NULL,
            secret_key  VARCHAR(255)  NOT NULL,
            PRIMARY KEY (user_id),
            UNIQUE (user_name)
        ) ENGINE=%s DEFAULT CHARSET=%s;`,
        ms.Database,
        ms.StoragEngine,
        ms.Charset,
    )
    if _, err := ms.Conn.Exec(stmt); err != nil {
        log.Printf("Unable to create table %s: %v\n", "users", err)
        return err
    }
    stmt = fmt.Sprintf(`
        CREATE TABLE IF NOT EXISTS %s.roles (
            role_id     BINARY(16)   NOT NULL,
            role_name   VARCHAR(64)  NOT NULL,
            PRIMARY KEY (role_id),
            UNIQUE (role_name)
        ) ENGINE=%s DEFAULT CHARSET=%s;`,
        ms.Database,
        ms.StoragEngine,
        ms.Charset,
    )
    if _, err := ms.Conn.Exec(stmt); err != nil {
        log.Printf("Unable to create table %s: %v\n", "roles", err)
        return err
    }
    stmt = fmt.Sprintf(`
        CREATE TABLE IF NOT EXISTS %s.user_roles (
            user_id  BINARY(16)  NOT NULL,
            role_id  BINARY(16)  NOT NULL,
            FOREIGN KEY(user_id) REFERENCES users(user_id),
            FOREIGN KEY(role_id) REFERENCES roles(role_id),
            PRIMARY KEY (user_id, role_id)
        ) ENGINE=%s DEFAULT CHARSET=%s;`,
        ms.Database,
        ms.StoragEngine,
        ms.Charset,
    )
    if _, err := ms.Conn.Exec(stmt); err != nil {
        log.Printf("Unable to create table %s: %v\n", "user_roles", err)
        return err
    }
    stmt = fmt.Sprintf(`
        CREATE TABLE IF NOT EXISTS %s.user_apikeys (
            user_id  BINARY(16)    NOT NULL,
            api_key  VARCHAR(255)  NOT NULL,
            FOREIGN KEY(user_id) REFERENCES users(user_id),
            PRIMARY KEY (user_id, api_key)
        ) ENGINE=%s DEFAULT CHARSET=%s;`,
        ms.Database,
        ms.StoragEngine,
        ms.Charset,
    )
    if _, err := ms.Conn.Exec(stmt); err != nil {
        log.Printf("Unable to create table %s: %v\n", "user_apikeys", err)
        return err
    }
    stmt = fmt.Sprintf(`
        CREATE TABLE IF NOT EXISTS %s.whitelist (
            id           INT(10)  UNSIGNED NOT NULL AUTO_INCREMENT,
            ip_address   INT(4)   UNSIGNED NOT NULL,
            subnet_mask  INT(4)   UNSIGNED NOT NULL,
            cidr         INT(2)   UNSIGNED NOT NULL,
            enabled      INT(1)   UNSIGNED NOT NULL,
            PRIMARY KEY (id),
            UNIQUE (ip_address, cidr)
        ) ENGINE=%s DEFAULT CHARSET=%s;`,
        ms.Database,
        ms.StoragEngine,
        ms.Charset,
    )
    if _, err := ms.Conn.Exec(stmt); err != nil {
        log.Printf("Unable to create table %s: %v\n", "wihtelist", err)
        return err
    }
    return nil
}

func (ms *MysqlKeyStorage) CreateDefaultRole(name string) error {
    stmt := fmt.Sprintf(`
            INSERT IGNORE INTO %s.roles (role_id, role_name)
            VALUES (UNHEX(REPLACE(UUID(),'-','')), ?)
        ;`,
        ms.Database,
    )
    if _, err := ms.Conn.Exec(stmt, name); err != nil {
        log.Printf("Unalbe to create role %s: %v\n", name, err)
        return err
    }
    return nil
}

func (ms *MysqlKeyStorage) CreateDefaultUser(name, secret string) error {
    if secret == "" {
        secret = GenerateSecretKey(16)
    }
    stmt := fmt.Sprintf(`
            INSERT IGNORE INTO %s.users (user_id, user_name, secret_key)
            VALUES (UNHEX(REPLACE(UUID(),'-','')), ?, ?)
        ;`,
        ms.Database,
    )
    if _, err := ms.Conn.Exec(stmt, name, secret); err != nil {
        log.Printf("Unalbe to create user %s: %v\n", name, err)
        return err
    }
    return nil
}

func (ms *MysqlKeyStorage) CreateUserCtx(ctx context.Context, name, secret string) error {
    if secret == "" {
        secret = GenerateSecretKey(16)
    }
    stmt := fmt.Sprintf(`
            INSERT INTO %s.users (user_id, user_name, secret_key)
            VALUES (UNHEX(REPLACE(UUID(),'-','')), ?, ?)
        ;`,
        ms.Database,
    )
    if _, err := ms.Conn.ExecContext(ctx, stmt, name, secret); err != nil {
        return err
    }
    return nil
}

func (ms *MysqlKeyStorage) GetUserIdByName(name string) (string, error) {
    var id string
    query := fmt.Sprintf(
        "SELECT HEX(user_id) FROM %s.users WHERE user_name = ?;",
        ms.Database,
    )
    row := ms.Conn.QueryRow(query, name)
    if err := row.Scan(&id); err != nil {
        return "", err
    }
    return id, nil
}

func (ms *MysqlKeyStorage) GetUserIdByNameCtx(ctx context.Context, name string) (string, error) {
    var id string
    query := fmt.Sprintf(
        "SELECT HEX(user_id) FROM %s.users WHERE user_name = ?;",
        ms.Database,
    )
    row := ms.Conn.QueryRowContext(ctx, query, name)
    if err := row.Scan(&id); err != nil {
        return "", err
    }
    return id, nil
}

func (ms *MysqlKeyStorage) DeleteUserByNameCtx(ctx context.Context, name string) error {
    /* remove oteher records related to this user in the user_roles, user_apikeys tables
          before delete user from users table. */
    stmt := fmt.Sprintf(`
        DELETE %[1]s.user_apikeys
        FROM %[1]s.user_apikeys
        INNER JOIN %[1]s.users
        ON %[1]s.user_apikeys.user_id = %[1]s.users.user_id
        WHERE %[1]s.users.user_name = ?
        ;`,
        ms.Database,
    )
    if _, err := ms.Conn.ExecContext(ctx, stmt, name); err != nil {
        log.Printf(
            "Unalbe to delete records related to this user %s in the user_apikeys table: %v\n",
            name, err)
        return err
    }
    stmt = fmt.Sprintf(`
        DELETE %[1]s.user_roles
        FROM %[1]s.user_roles
        INNER JOIN %[1]s.users
        ON %[1]s.user_roles.user_id = %[1]s.users.user_id
        WHERE %[1]s.users.user_name = ?
        ;`,
        ms.Database,
    )
    if _, err := ms.Conn.ExecContext(ctx, stmt, name); err != nil {
        log.Printf(
            "Unalbe to delete records related to this user %s in the user_roles table: %v\n",
            name, err)
        return err
    }

    stmt = fmt.Sprintf(
        "DELETE FROM %s.users WHERE user_name = ?;",
        ms.Database,
    )
    if _, err := ms.Conn.ExecContext(ctx, stmt, name); err != nil {
        log.Printf("Unalbe to delete user %s in the users table: %v\n", name, err)
        return err
    }
    return nil
}

func (ms *MysqlKeyStorage) GetSecretByUsernameCtx(ctx context.Context, name string) (string, error) {
    var secret string
    query := fmt.Sprintf(
        "SELECT secret_key FROM %s.users WHERE user_name = ?;",
        ms.Database,
    )
    row := ms.Conn.QueryRowContext(ctx, query, name)
    if err := row.Scan(&secret); err != nil {
        return "", err
    }
    return secret, nil
}

func (ms *MysqlKeyStorage) UpdateSecretByUsernameCtx(ctx context.Context, name, secret string) error {
    if secret == "" {
        secret = GenerateSecretKey(16)
    }
    stmt := fmt.Sprintf(
        "UPDATE %s.users SET secret_key = ? WHERE user_name = ?;",
        ms.Database,
    )
    if _, err := ms.Conn.ExecContext(ctx, stmt, secret, name); err != nil {
        return err
    }
    return nil
}

func (ms *MysqlKeyStorage) UpdateUsernameCtx(ctx context.Context, old, new string) error {
    stmt := fmt.Sprintf(
        "UPDATE %s.users SET user_name = ? WHERE user_name = ?",
        ms.Database,
    )
    if new == "" {
        log.Println("New user name can't be empty.")
        return fmt.Errorf("New user name can't be empty.\n")
    }
    if _, err := ms.Conn.ExecContext(ctx, stmt, new, old); err != nil {
        log.Printf("SQL Update Query Error: %v\n", err)
        return err
    }
    return nil
}

func (ms *MysqlKeyStorage) GetAllUsers(ctx context.Context, limits int) ([]string, error) {
    var (
        list  []string
        query string
    )
    if limits <= 0 {
        query = fmt.Sprintf(
            "SELECT user_name FROM %s.users ORDER BY user_name;",
            ms.Database,
        )
    } else {
        query = fmt.Sprintf(
            "SELECT user_name FROM %s.users ORDER BY user_name LIMIT %d;",
            ms.Database,
            limits,
        )
    }
    rows, err := ms.Conn.QueryContext(ctx, query)
    if err != nil {
        return nil, err
    }
    defer rows.Close()
    for rows.Next() {
        var user string
        if err = rows.Scan(&user); err != nil {
            return nil, err
        }
        list = append(list, user)
    }
    return list ,err
}

func (ms *MysqlKeyStorage) CreateRoleCtx(ctx context.Context, name string) error {
    stmt := fmt.Sprintf(`
        INSERT INTO %s.roles (role_id, role_name)
        VALUES (UNHEX(REPLACE(UUID(),'-','')), ?)
        ;`,
        ms.Database,
    )
    if _, err := ms.Conn.ExecContext(ctx, stmt, name); err != nil {
        return err
    }
    return nil
}

func (ms *MysqlKeyStorage) GetRoleIdByNameCtx(ctx context.Context, name string) (string, error) {
    var id string
    query := fmt.Sprintf(
        "SELECT HEX(role_id) FROM %s.roles WHERE role_name = ?;",
        ms.Database,
    )
    row := ms.Conn.QueryRowContext(ctx, query, name)
    if err := row.Scan(&id); err != nil {
        return "", err
    }
    return id, nil
}

func (ms *MysqlKeyStorage) GetAllRolesCtx(ctx context.Context, limits int) ([]string, error) {
    var (
        list  []string
        name  string
        query string
    )
    if limits <= 0 {
        query = fmt.Sprintf(
            "SELECT role_name FROM %s.roles ORDER BY role_name;",
            ms.Database,
        )
    } else {
        query = fmt.Sprintf(
            "SELECT role_name FROM %s.roles ORDER BY role_name LIMIT %d;",
            ms.Database,
            limits,
        )
    }
    rows, err := ms.Conn.QueryContext(ctx, query)
    defer rows.Close()
    for rows.Next() {
        if err = rows.Scan(&name); err != nil {
            return nil, err
        }
        list = append(list, name)
    }
    return list ,err
}

func (ms *MysqlKeyStorage) DeleteRoleByNameCtx(ctx context.Context, name string) error {
    /* remove oteher records related to this role in the user_roles tables
          before delete role from roles table. */
    stmt := fmt.Sprintf(`
        DELETE %[1]s.user_roles
        FROM %[1]s.user_roles
        INNER JOIN %[1]s.roles
        ON %[1]s.user_roles.role_id = %[1]s.roles.role_id
        WHERE %[1]s.roles.role_name = ?
        ;`,
        ms.Database,
    )
    if _, err := ms.Conn.ExecContext(ctx, stmt, name); err != nil {
        log.Printf(
            "Unalbe to delete records related to this role %s in the user_roles table: %v\n",
            name, err)
        return err
    }

    stmt = fmt.Sprintf(
        "DELETE FROM %s.roles WHERE role_name = ?;",
        ms.Database,
    )
    if _, err := ms.Conn.ExecContext(ctx, stmt, name); err != nil {
        log.Printf("Unalbe to delete role %s in the roles table: %v\n", name, err)
        return err
    }
    return nil
}

func (ms *MysqlKeyStorage) GrantRolesToUser(user string, roles ...string) error {
    var (
        stmt  string
        err   error
    )
    for _, rl := range roles {
        stmt = fmt.Sprintf(`
            INSERT IGNORE INTO %[1]s.user_roles
              (user_id, role_id)
            SELECT us.user_id, rs.role_id
            FROM %[1]s.users AS us
            INNER JOIN %[1]s.roles AS rs
            WHERE us.user_name = ?
            AND rs.role_name = ?
            ;`,
            ms.Database,
        )
        _, err = ms.Conn.Exec(stmt, user, rl)
        if err != nil {
            log.Printf("Unable to grant role %s to user %s: %v\n", rl, user, err)
            return err
        }
    }
    return nil
}

func (ms *MysqlKeyStorage) GrantRolesToUserCtx(ctx context.Context, user string, roles ...string) error {
    var (
        stmt  string
        err   error
    )
    for _, rl := range roles {
        stmt = fmt.Sprintf(`
            INSERT IGNORE INTO %[1]s.user_roles
              (user_id, role_id)
            SELECT us.user_id, rs.role_id
            FROM %[1]s.users AS us
            INNER JOIN %[1]s.roles AS rs
            WHERE us.user_name = ?
            AND rs.role_name = ?
            ;`,
            ms.Database,
        )
        _, err = ms.Conn.ExecContext(ctx, stmt, user, rl)
        if err != nil {
            return err
        }
    }
    return nil
}

func (ms *MysqlKeyStorage) RemoveRolesFromUserCtx(ctx context.Context, user string, roles ...string) error {
    var (
        stmt  string
        err   error
    )
    for _, rl := range roles {
        stmt = fmt.Sprintf(`
            DELETE %[1]s.user_roles
            FROM %[1]s.user_roles
            INNER JOIN %[1]s.roles
            ON %[1]s.roles.role_id = %[1]s.user_roles.role_id
            INNER JOIN %[1]s.users
            ON %[1]s.users.user_id = %[1]s.user_roles.user_id
            WHERE %[1]s.users.user_name = ?
            AND %[1]s.roles.role_name = ?
            ;`,
            ms.Database,
        )
        _, err = ms.Conn.ExecContext(ctx, stmt, user, rl)
        if err != nil {
            return err
        }
    }
    return nil
}

func (ms *MysqlKeyStorage) GetRolesBelongToUserCtx(ctx context.Context, user string) ([]string, error) {
    var (
        list []string
        role  string
    )
    query := fmt.Sprintf(`
        SELECT role_name
        FROM %[1]s.roles
        INNER JOIN %[1]s.user_roles ON %[1]s.user_roles.role_id = %[1]s.roles.role_id
        INNER JOIN %[1]s.users ON %[1]s.user_roles.user_id = %[1]s.users.user_id
        WHERE %[1]s.users.user_name = ?
        ;`,
        ms.Database,
    )
    rows, err := ms.Conn.QueryContext(ctx, query, user)
    if err != nil {
        return nil, err
    }
    defer rows.Close()
    for rows.Next() {
        if err = rows.Scan(&role); err != nil {
            return nil, err
        }
        list = append(list, role)
    }
    return list ,err
}

func (ms *MysqlKeyStorage) CheckRoleForUserCtx(ctx context.Context, user, role string) (bool, error) {
    var check int
    query := fmt.Sprintf(`
        SELECT EXISTS (
            SELECT 1
            FROM %[1]s.user_roles
            INNER JOIN %[1]s.users
            ON (%[1]s.users.user_id = %[1]s.user_roles.user_id)
            INNER JOIN %[1]s.roles
            ON (%[1]s.roles.role_id = %[1]s.user_roles.role_id)
            WHERE %[1]s.users.user_name = ?
            AND %[1]s.roles.role_name = ?
            LIMIT 1
        );`,
        ms.Database,
    )
    row := ms.Conn.QueryRow(query, user, role)
    if err := row.Scan(&check); err != nil {
        return false, err
    }
    if check != 1 {
        return false, nil
    }
    return true, nil
}

func (ms *MysqlKeyStorage) AttachApiKeyToUser(user, key string) error {
    stmt := fmt.Sprintf(`
        INSERT INTO %[1]s.user_apikeys
            (user_id, api_key)
        VALUES (
            (SELECT user_id FROM %[1]s.users WHERE user_name = '%s'), ?
        );`,
        ms.Database,
        user,
    )
    if _, err := ms.Conn.Exec(stmt, key); err != nil {
        log.Printf("Unable to attach the given scret key to user %s: %v\n", user, err)
        return err
    }
    return nil
}

func (ms *MysqlKeyStorage) AttachApiKeyToUserCtx(ctx context.Context, user, key string) error {
    stmt := fmt.Sprintf(`
        INSERT INTO %[1]s.user_apikeys
            (user_id, api_key)
        VALUES ((SELECT user_id FROM %[1]s.users WHERE user_name = '%s'), ?)
        ;`,
        ms.Database,
        user,
    )
    if _, err := ms.Conn.ExecContext(ctx, stmt, key); err != nil {
        log.Printf("Unable to attach the given scret key to user %s: %v\n", user, err)
        return err
    }
    return nil
}

func (ms *MysqlKeyStorage) UpdateApiKeyForUserCtx(ctx context.Context, user, key string) error {
    stmt := fmt.Sprintf(`
        UPDATE %[1]s.user_apikeys
        INNER JOIN %[1]s.user_apikeys
        ON %[1]s.user_apikeys.user_id = %[1]s.users.user_id
        SET api_key = ?
        WHERE %[1]s.user_name = ?
        ;`,
        ms.Database,
    )
    if _, err := ms.Conn.ExecContext(ctx, stmt, key, user); err != nil {
        log.Printf("Unable to update the scret key of user %s: %v\n", user, err)
        return err
    }
    return nil
}

func (ms *MysqlKeyStorage) GetUserApiKeysCtx(ctx context.Context, name string) ([]string, error) {
    var (
        list []string
        key    string
    )
    query := fmt.Sprintf(`
        SELECT api_key
        FROM %[1]s.user_apikeys
        INNER JOIN %[1]s.users
        ON (%[1]s.users.user_id = %[1]s.user_apikeys.user_id)
        WHERE %[1]s.users.user_name = ?
        ;`,
        ms.Database,
    )
    rows, err := ms.Conn.QueryContext(ctx, query, name)
    if err != nil {
        log.Printf("Unable to get all api keys belong to the user %s: %v\n", name, err)
        return nil, err
    }
    defer rows.Close()
    for rows.Next() {
        err = rows.Scan(&key)
        if err != nil {
            return nil, err
        }
        list = append(list, key)
    }
    return list, nil
}

func (ms *MysqlKeyStorage) CheckUserApiKeysExist(name string) (bool, error) {
    var result int
    query := fmt.Sprintf(`
        SELECT EXISTS(
            SELECT 1
            FROM %[1]s.user_apikeys
            INNER JOIN %[1]s.users
            ON (%[1]s.users.user_id = %[1]s.user_apikeys.user_id)
            WHERE %[1]s.users.user_name = ?
            LIMIT 1
        );`,
        ms.Database,
    )
    row := ms.Conn.QueryRow(query, name)
    if err := row.Scan(&result); err != nil {
        return false, err
    }
    if result != 1 {
        return false, nil
    }
    return true, nil
}

func (ms *MysqlKeyStorage) AddCidrToWhitelist(cidr *IPv4CIDR) error {
    stmt := fmt.Sprintf(`
        INSERT IGNORE INTO %s.whitelist
        SET ip_address = INET_ATON('%s'),
        subnet_mask = CONV(CONCAT(REPEAT(1,%[3]d),REPEAT(0,32-%[3]d)),2,10),
        cidr = %[3]d,
        enabled = ?
        ;`,
        ms.Database,
        cidr.IPAddress,
        cidr.CIDR,
    )
    if _, err := ms.Conn.Exec(stmt, 1); err != nil {
        log.Printf("Unable to add ipv4 address cidr to the whitelist: %v", err)
        return err
    }
    return nil
}

func (ms *MysqlKeyStorage) AddCidrToWhitelistCtx(ctx context.Context, cidr *IPv4CIDR) error {
    stmt := fmt.Sprintf(`
        INSERT IGNORE INTO %s.whitelist
        SET ip_address = INET_ATON('%s'),
        subnet_mask = CONV(CONCAT(REPEAT(1,%[3]d),REPEAT(0,32-%[3]d)),2,10),
        cidr = %[3]d,
        enabled = ?
        ;`,
        ms.Database,
        cidr.IPAddress,
        cidr.CIDR,
    )
    if _, err := ms.Conn.ExecContext(ctx, stmt, 1); err != nil {
        log.Printf("Unable to add ipv4 address cidr to the whitelist: %v", err)
        return err
    }
    return nil
}

func (ms *MysqlKeyStorage) AddDefaultCidrsToWhitelist(cidrs ...*IPv4CIDR) error {
    var err error
    // add the "0.0.0.0/0" for bypass whitelist function fisrt
    bypasss := &IPv4CIDR {
        IPAddress: "0.0.0.0",
        CIDR: 0,
    }
    if err = ms.AddCidrToWhitelist(bypasss); err != nil {
        return err
    }
    // append more default cidr items to whitelist
    for _, cdr := range cidrs {
        if err = ms.AddCidrToWhitelist(cdr); err != nil {
            return err
        }
    }
    return nil
}

func (ms *MysqlKeyStorage) DeleteCidrFromWhitelistCtx(ctx context.Context, cidr *IPv4CIDR) error {
    stmt := fmt.Sprintf(`
        DELETE FROM %s.whitelist
        WHERE ip_address = INET_ATON('%s')
        AND cidr = ?
        ;`,
        ms.Database,
        cidr.IPAddress,
    )
    if _, err := ms.Conn.ExecContext(ctx, stmt, cidr.CIDR); err != nil {
        log.Printf("Unable to remove ipv4 address cidr from the whitelist: %v", err)
        return err
    }
    return nil
}

func (ms *MysqlKeyStorage) ToggleCidr(ctx context.Context, cidr *IPv4CIDR, enable int) error {
    // enable = 0 to disable, and 1 to enable
    stmt := fmt.Sprintf(`
        UPDATE %s.whitelist
        SET enabled = ?
        WHERE ip_address = INET_ATON('%s')
        AND cidr = ?
        ;`,
        ms.Database,
        cidr.IPAddress,
    )
    if _, err := ms.Conn.ExecContext(ctx, stmt, enable, cidr.CIDR); err != nil {
        if enable == 0 {
            log.Printf("Unable to disable the cidr in whitelist: %v", err)
        }
        if enable == 1 {
            log.Printf("Unable to enable the cidr in whitelist: %v", err)
        }
        return err
    }
    return nil
}

func (ms *MysqlKeyStorage) GetWhitelist() ([]string, error) {
    var (
        list []string
        item  string
    )
    query := fmt.Sprintf(
        "SELECT CONCAT(INET_NTOA(ip_address), '/', cidr) FROM %s.whitelist WHERE enabled = ?;",
        ms.Database,
    )
    rows, err := ms.Conn.Query(query, 1)
    if err != nil {
        log.Printf("Unable to get whitelist: %v\n", err)
        return nil, err
    }
    defer rows.Close()
    for rows.Next() {
        err = rows.Scan(&item)
        if err != nil {
            log.Printf("Query Result Scan Error: %v\n", err)
            return nil, err
        }
        list = append(list, item)
    }
    return list, nil
}

func (ms *MysqlKeyStorage) GetWhitelistCtx(ctx context.Context) ([]string, error) {
    var (
        list []string
        item  string
    )
    query := fmt.Sprintf(
        "SELECT CONCAT(INET_NTOA(ip_address), '/', cidr) FROM %s.whitelist WHERE enabled = ?;",
        ms.Database,
    )
    rows, err := ms.Conn.QueryContext(ctx, query, 1)
    if err != nil {
        log.Printf("Unable to get whitelist: %v\n", err)
        return nil, err
    }
    defer rows.Close()
    for rows.Next() {
        err = rows.Scan(&item)
        if err != nil {
            log.Printf("Query Result Scan Error: %v\n", err)
            return nil, err
        }
        list = append(list, item)
    }
    return list, nil
}
