package main

import (
    "log"
    "gopkg.in/yaml.v3"
    "io/ioutil"
    "gopkg.in/natefinch/lumberjack.v2"

    "totp-auth-server/pkg/totp"
)

type config struct {
    ServerConfig    *serverDefaults       `yaml:"default"`
    TotpConfig      *totp.TotpProperties  `yaml:"totp"`
    DatabaseConfig  *dbSettings           `yaml:"database"`
    SyslogConfig    *syslogSettings       `yaml:"syslog"`
    LogConfig       *lumberjack.Logger    `yaml:"log"`
    QRcodeConfig    *qrcodeSettings       `yaml:"qrcode"`
}

type serverDefaults struct {
    User             string    `yaml:"user"`
    Key              string    `yaml:"key"`
    ApiKey           string    `yaml:"api_key"`
    ApiSecret        string    `yaml:"api_secret"`
    Role             string    `yaml:"role"`
    RealIpHeader     string    `yaml:"real_ip_header"`
    AdminRoles       []string  `yaml:"admin_roles"`
    Whitelist        []string  `yaml:"whitelist"`
    MaxAllowedConns  int       `yaml:"max_allowed_conns"`
    ReadTimeout      int       `yaml:"read_timeout"`
    WriteTimeout    int        `yaml:"write_timeout"`
    MaxHeaderBytes   int       `yaml:"max_header_bytes"`
}

type qrcodeSettings struct {
    Level  string  `yaml:"level"`
    Size   int     `yaml:"size"`
}

type dbSettings struct {
    Type             string  `yaml:"type"`
    Protocol         string  `yaml:"protocol"`
    Host             string  `yaml:"host"`
    Port             int     `yaml:"port"`
    User             string  `yaml:"user"`
    Password         string  `yaml:"password"`
    Database         string  `yaml:"database"`
    Charset          string  `yaml:"charset"`
    ConnMaxLifetime  int     `yaml:"conn_max_lifetime"`
    MaxOpenConns     int     `yaml:"max_open_conns"`
    MaxIdleConns     int     `yaml:"max_idle_conns"`
}

type syslogSettings struct {
    protocol  string  `yaml:"protocol"`
    host      string  `yaml:"host"`
    port      int     `yaml:"port"`
}

func parseConfig(filename string) *config {
    var cfg config
    data, err := ioutil.ReadFile(filename)
    if err != nil {
        log.Fatal(err)
    }
    if err = yaml.Unmarshal(data, &cfg); err != nil {
        log.Fatal(err)
    }
    if cfg.ServerConfig.ReadTimeout <= 0 {
        cfg.ServerConfig.ReadTimeout = 10
    }
    if cfg.ServerConfig.WriteTimeout <= 0 {
        cfg.ServerConfig.WriteTimeout = 30
    }
    if cfg.ServerConfig.MaxHeaderBytes <= 0 {
        cfg.ServerConfig.MaxHeaderBytes = 2
    }
    return &cfg
}
