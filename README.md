# TOTP Auth Server

A simple totp auth server written in golang.

## Build

Install go package and build build-essential pacakge, enter the source directory nad run the following to build:

```bash
go build -o bin/totp-auth-server -mod mod
```

You may need to enable the swagger api doc for debug in the testing environment by the "-tags doc" parameter:

```bash
go build -o bin/totp-auth-server -mod mod -tags doc
```

Detemeter the version abd build date for service by -ldflags parameter, such as:

```bash
VERSION='1.0.4'
BUILD_DATE=$(date +%F)
go build -o test/totp-auth-server -mod mod -ldflags "-X main.Version=${VERSION} -X main.Build=${BUILD_DATE}"
```

Or you could run the script in the scripts/ folder to build a binary to test/ directory for testing, and you will find a named totp-auth-server executable file in the test/ after run the script :

```bash
sh scripts/build_bin.sh
```

Show the help information with '-h':

```bash
./totp-auth-server -h
```

## Deployment

### Run Serive as Docker Container

Install docker and docker-compose first, and run the docker daemon:

```bash
sudo systemctl start docker
```

Edit the docker-compose.yml file, change the config.yaml path if you want to user other config.yaml, or remove the "DISABLE_SWAGGER" env to enable swagger doc for debug.

And the run the following command to build docker image and startup the service:

```bash
docker-compose up -d -f docker-compose.yaml
```

## Basic Usage

You could get api-key and api-secret for the default admin from logs if you left blank on the api_key and api_secret field in the config.yaml. It should like this:

```
totp-auth  | [totp-auth-server] YYYY/MM/DD HH:MM:SS main.go:109: Auto-generate api-key: 7aa......15c
totp-auth  | [totp-auth-server] YYYY/MM/DD HH:MM:SS main.go:113: Auto-generate api-secret: b24......42e
```

And get the admin qrcode by api-key and api-secret:

```bash
curl -X 'GET' \
  'http://localhost:9987/manage/user/admin/qrcode' \
  -H 'accept: image/png' \
  -H 'X-API-Key: YOUR_ADMIN_API_KEY' \
  -H 'X-Secret-Key: YOUR_ADMIN_API_SECRET' \
  -H 'User-Name: admin' \
  --output totp-admin-qrcode.png
```

Scan the qrcode png by FreeOTP or Google Authenticator, and use pin code to verify totp authentication:

```bash
curl -X 'GET' \
  'http://localhost:9987/auth/verify' \
  -H 'accept: application/json' \
  -H 'TOTP-PIN-CODE: YOUR_PIN_CODE' \
  -H 'TOTP-USER: admin'
```

Create a new role "test":

```bash
curl -X 'POST' \
  'http://localhost:9987/manage/role/test' \
  -H 'accept: application/json' \
  -H 'X-API-Key: YOUR_ADMIN_API_KEY' \
  -H 'X-Secret-Key: YOUR_ADMIN_API_SECRET' \
  -H 'User-Name: admin' \
```

Check the new role has been created, and it will show a list which contains test role:

```bash
curl -X 'GET' \
  'http://localhost:9987/manage/roles' \
  -H 'accept: application/json' \
  -H 'X-API-Key: YOUR_ADMIN_API_KEY' \
  -H 'X-Secret-Key: YOUR_ADMIN_API_SECRET' \
  -H 'User-Name: admin'
```

Create a new user "tester":

```bash
curl -X 'POST' \
  'http://localhost:9987/manage/user/tester' \
  -H 'accept: application/json' \
  -H 'X-API-Key: YOUR_ADMIN_API_KEY' \
  -H 'X-Secret-Key: YOUR_ADMIN_API_SECRET' \
  -H 'User-Name: admin'
```

Grant the "test" to user "tester":

```bash
curl -X 'POST' \
  'http://localhost:9987/manage/role/test/user/tester' \
  -H 'accept: application/json' \
  -H 'X-API-Key: YOUR_ADMIN_API_KEY' \
  -H 'X-Secret-Key: YOUR_ADMIN_API_SECRET' \
  -H 'User-Name: admin'
```

Get the qrcode png of user "tester", and scan it by mobile app:

```bash
curl -X 'GET' \
  'http://localhost:9987/manage/user/tester/qrcode' \
  -H 'accept: image/png' \
  -H 'X-API-Key: YOUR_ADMIN_API_KEY' \
  -H 'X-Secret-Key: YOUR_ADMIN_API_SECRET' \
  -H 'User-Name: admin' \
  --output totp-tester-qrcode.png
```

Now you could use pin code to verify as tester user:

```bash
curl -X 'GET' \
  'http://localhost:9987/auth/verify' \
  -H 'accept: application/json' \
  -H 'TOTP-PIN-CODE: TESTER_PIN_CODE' \
  -H 'TOTP-USER: tester'
```

## TOTP Authenticator

- FreeOTP

FreeOTP is a open source totp authenticator, and more flexiable.

- Google Authenticator

Google Authenticator is most popular tool, but it's android version has some limits:

- algorithm only support sha1, app will ignore the algorithm setting in the totp auth url
- time step always is 30 seconds, app will ignore the period setting in the totp auth url
- length only be 6-8 digits, app will ignore the digits setting in the totp auth url

## Tolerance Mode

Google authenticator ignores time step setting from totp url. In some case you need to verify one time password with longer time step by it. Enable the tolerance mode allow user could verify successfully for one minute or 30 seconds after one time password was expired.

To enable tolerance mode, edit config.yaml and change the following options in the totp section:

```yaml
totp:
  algorithm:      sha1
  timestep:       30
  digits:         6
  tolerance_mode: true
  tolerant_steps: 1
  shift_seconds:  1
```

The tolerant_steps option above means how many times to back in time by time step, and shift_seconds option just avoid to exceed the right period.

## License

This application is releaded under MIT License.
