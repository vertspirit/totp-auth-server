package main

import (
    "fmt"
    "log"
    "strings"
    "strconv"
    "io"
    "os"
    "github.com/gin-gonic/gin"

    "totp-auth-server/api"
    "totp-auth-server/pkg/totp"
    "totp-auth-server/pkg/keygen"
)

var (
    Version    string
    Build      string
    settings   *args
    cfg        *config
    swagHdler  gin.HandlerFunc
)

type TotpApiSettings = api.TotpApiSettings

func init() {
    settings = parseFlags()
    cfg = parseConfig(settings.config)

    if cfg.LogConfig.Filename == "" {
        cfg.LogConfig.Filename = "/var/log/totp-auth-server/totp-auth-server.log"
    }
    mw := io.MultiWriter(os.Stdout, cfg.LogConfig)
    log.SetOutput(mw)

    srvName := fmt.Sprintf("[%s] ", "totp-auth-server")
    log.SetPrefix(srvName)
    log.SetFlags(log.LstdFlags|log.Lshortfile)
    if settings.syslog {
        logger, err := cfg.SyslogConfig.SetupSysLog(srvName)
        if err != nil {
            mw = io.MultiWriter(cfg.LogConfig, logger)
            log.SetOutput(mw)
        } else {
            log.Printf("Fail to setup syslog writer for logging: %v", err)
        }
    }
}

// @title                      TOTP-Auth API Doc
// @version                    1.0
// @description                Swagger api doc for totp-auth-server project.
// @termsOfService             http://swagger.io/terms/
// @contact.name               Dongjun Wu
// @contact.email              ziyawu@gmail.com
// @license.name               MIT
// @license.url                https://gitlab.com/vertspirit/totp-auth-server/blob/master/LICENSE
// @securityDefinitions.apikey ApiKeyAuth
// @in                         header
// @name                       X-API-Key
// @securityDefinitions.apikey ApiSecretAuth
// @in                         header
// @name                       X-Secret-Key
// @securityDefinitions.apikey ApiUserAuth
// @in                         header
// @name                       User-Name
// @schemes                    http https
// @BasePath                   /

func main() {
    ks := &totp.MysqlKeyStorage {
        Host:            cfg.DatabaseConfig.Host,
        Port:            cfg.DatabaseConfig.Port,
        Protocol:        cfg.DatabaseConfig.Protocol,
        User:            cfg.DatabaseConfig.User,
        Password:        cfg.DatabaseConfig.Password,
        Database:        cfg.DatabaseConfig.Database,
        Charset:         cfg.DatabaseConfig.Charset,
        ConnMaxLifetime: cfg.DatabaseConfig.ConnMaxLifetime,
        MaxOpenConns:    cfg.DatabaseConfig.MaxOpenConns,
        MaxIdleConns:    cfg.DatabaseConfig.MaxIdleConns,
    }
    tprop := cfg.TotpConfig
    ctler := &totp.Controller {
        TotpProperies: tprop,
    }
    ctler.MapKeyStorage(ks)

    qrcode := &totp.QRcodeConfig {
        Level: cfg.QRcodeConfig.Level,
        Size:  cfg.QRcodeConfig.Size,
    }

    otpServer := &TotpApiSettings {
        Version:          Version,
        Controller:       ctler,
        KeyStorage:       ks,
        QRcodeSettings:   qrcode,
        RealIpHeader:     cfg.ServerConfig.RealIpHeader,
        AdminRoles:       cfg.ServerConfig.AdminRoles,
        MaxAllowedConns:  cfg.ServerConfig.MaxAllowedConns,
        ReadTimeout:      cfg.ServerConfig.ReadTimeout,
        WriteTimeout:     cfg.ServerConfig.WriteTimeout,
        MaxHeaderBytes:   cfg.ServerConfig.MaxHeaderBytes,
        EnablePprof:      settings.debug,
    }
    if settings.subpath != "" {
        otpServer.SubpathPrefix = settings.subpath
    }

    otpServer.KeyStorage.CreateConnection()
    defer otpServer.KeyStorage.CloseConnection()
    otpServer.KeyStorage.CreateInitDatabase()
    otpServer.KeyStorage.CreateInitTables()
    otpServer.KeyStorage.CreateDefaultRole("admin")
    otpServer.KeyStorage.CreateDefaultUser(cfg.ServerConfig.User, cfg.ServerConfig.Key)
    otpServer.KeyStorage.GrantRolesToUser(cfg.ServerConfig.User, cfg.ServerConfig.Role)
    checkDefaultUser, _ := otpServer.KeyStorage.CheckUserApiKeysExist(cfg.ServerConfig.User)
    if ! checkDefaultUser {
        if cfg.ServerConfig.ApiKey == "" {
            cfg.ServerConfig.ApiKey = keygen.GenerateRandomSalt(16)
            log.Println(">>> Please save api-key and api-secret carefully. <<<")
            log.Printf("Auto-generate api-key: %s\n", cfg.ServerConfig.ApiKey)
        }
        if cfg.ServerConfig.ApiSecret == "" {
            cfg.ServerConfig.ApiSecret = keygen.GenerateRandomSalt(8)
            log.Printf("Auto-generate api-secret: %s\n", cfg.ServerConfig.ApiSecret)
        }
        hash := keygen.HashPasswordWithSaltBySha("sha512", cfg.ServerConfig.ApiKey, cfg.ServerConfig.ApiSecret)
        otpServer.KeyStorage.AttachApiKeyToUser(cfg.ServerConfig.User, hash)
    }

    var cidrs []*totp.IPv4CIDR
    if cfg.ServerConfig.Whitelist != nil {
        for _, item := range cfg.ServerConfig.Whitelist {
            ip := strings.Split(item ,"/")
            bitCount, err := strconv.Atoi(ip[1])
            if err != nil {
              log.Printf("Fail to convert cidr to integer: %v", err)
              continue
            }
            if bitCount > 32 {
              log.Println("CIDR can't be larger than 32.")
              continue
            }
            cidr := &totp.IPv4CIDR {
                IPAddress: ip[0],
                CIDR: bitCount,
            }
            cidrs = append(cidrs, cidr)
        }
    }
    otpServer.KeyStorage.AddDefaultCidrsToWhitelist(cidrs...)
    cuurentWhitelist, err := otpServer.KeyStorage.GetWhitelist()
    if err != nil {
        log.Printf("Fail to get the whitelist from database: %v", err)
        cuurentWhitelist = []string{"0.0.0.0/0"}
    }
    otpServer.Whitelist = cuurentWhitelist

    tz := os.Getenv("TZ")
    fmt.Printf("[%s] Version:    %s\n", "TOTP Auth Server", Version)
    fmt.Printf("[%s] Build Date: %s\n", "TOTP Auth Server", Build)
    fmt.Printf("[%s] Time Zone:  %s\n", "TOTP Auth Server", tz)

    addr := fmt.Sprintf("%s:%d", settings.host, settings.port)
    otpServer.StartupServer(addr, swagHdler, settings.graceful)
}
