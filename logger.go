package main

import (
    "log"
    "log/syslog"
    "fmt"
)

func (ss *syslogSettings) SetupSysLog(name string) (*syslog.Writer, error) {
    addr := ""
    if ss.host != "" && ss.port != 0 {
        addr = fmt.Sprintf("%s:%d", ss.host, ss.port)
    }
    if ss.protocol == "" {
        ss.protocol = "tcp"
    }
    logger, err := syslog.Dial(ss.protocol, addr, syslog.LOG_ERR|syslog.LOG_DAEMON, name)
    if err != nil {
        log.Printf("Fail to connect to syslog server: %v", err)
        return nil, err
    }
    return logger, nil
}
