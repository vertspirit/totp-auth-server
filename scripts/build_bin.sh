#!/bin/sh

STARTDIR=$(dirname "$(readlink -f $0)")
cd "${STARTDIR}/.."

read -p 'Enter the version [1.0.4]: ' VERSION
VERSION=${VERSION:-1.0.4}
BUILD_DATE=$(date +%F)

go build -o test/totp-auth-server -mod mod -tags doc \
  -ldflags "-X main.Version=${VERSION} -X main.Build=${BUILD_DATE}"
